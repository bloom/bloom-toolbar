/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/* bloom-toolbar.h */
/*
 * Copyright (c) 2009, 2010 Agorabox.
 *
 * Authors: Bastien Bouzerau <bastien.bouzerau@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _BLOOM_TOOLBAR
#define _BLOOM_TOOLBAR

#include "bloom-panel.h"
#include "../libbloom-toolbar/libbloom-toolbar.h"

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BLOOM_TYPE_TOOLBAR                 (bloom_toolbar_get_type())
#define BLOOM_TOOLBAR(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLOOM_TYPE_TOOLBAR, BloomToolbar))
#define BLOOM_TOOLBAR_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), BLOOM_TYPE_TOOLBAR, BloomToolbarClass))
#define BLOOM_IS_TOOLBAR(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLOOM_TYPE_TOOLBAR))
#define BLOOM_IS_TOOLBAR_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), BLOOM_TYPE_TOOLBAR))
#define BLOOM_TOOLBAR_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), BLOOM_TYPE_TOOLBAR, BloomToolbarClass))

typedef struct _name_and_style name_and_style;

typedef struct _BloomToolbar BloomToolbar;
typedef struct _BloomToolbarClass BloomToolbarClass;

typedef struct _BloomToolbarPrivate BloomToolbarPrivate;

extern guint signals_p[];

enum
{
    MYZONE = 0,
    INTERNET_ZONE,
    MEDIA_ZONE,
    APPS_ZONE,
    DEVICE_ZONE,
    MAIL_ZONE,

    APPLETS_START,

    /* Below here are the applets -- with the new dbus API, these are
     * just extra panels, only the buttons are slightly different in size.
     */
    SYNC_APPLET = APPLETS_START,
    WIFI_APPLET, 
    VOLUME_APPLET,
    BATTERY_APPLET,
    TOGGLE_APPLET,
    BT_APPLET,
    SHUTDOWN_APPLET,

    /* LAST */
    NUM_ZONES
};

struct _name_and_style
{
    const gchar *name;
    const gchar *style;
    const gchar *style_active;
};

struct _BloomToolbar
{
    GObject              parent;
    gchar               *name;
    GSList              *pending_panels;
    BloomToolbarPrivate *priv;
};

typedef enum
{
  MNB_TOOLBAR_PANEL_NORMAL = 0,
  MNB_TOOLBAR_PANEL_APPLET,
  MNB_TOOLBAR_PANEL_CLOCK
} MnbToolbarPanelType;

typedef struct
{
  gchar      *name;
  gchar      *service;
  gchar      *button_stylesheet;
  gchar      *tooltip;
  const gchar *button_style;
  const gchar  *active_button_style;

  GtkWindow  *show_loading;
  gboolean    request_show;

  MnbToolbarPanelType type;

  gboolean    windowless : 1; /* button-only panel */
  gboolean    unloaded   : 1;
  gboolean    current    : 1;
  gboolean    pinged     : 1;
  gboolean    required   : 1;
  gboolean    failed     : 1;
} ToolbarPendingPanel;

struct _BloomToolbarClass
{
    GObjectClass                     parent;
    GObject                         *instance;

    void (*request_button_style)    (BloomToolbar *toolbar, const gchar *style);
    void (*request_tooltip)         (BloomToolbar *toolbar, const gchar *style);
    void (*request_show)            (BloomToolbar *toolbar);
    void (*request_unactive_buttons)(BloomToolbar *toolbar);
    void (*request_active_buttons)  (BloomToolbar *toolbar);
    void (*clicked)                 (BloomToolbar *toolbar, const gchar *name);
    void (*remote_process_died)     (BloomToolbar *toolbar);
};

GType bloom_toolbar_get_type            (void);
BloomToolbar* bloom_toolbar_new         (void);
gint bloom_toolbar_panel_name_to_index  (const gchar *name);
GSList *bloom_toolbar_get_pids          (BloomToolbar *toolbar);

void bloom_toolbar_hide_all_panels      (BloomToolbar *toolbar);
void bloom_toolbar_setup_panels         (BloomToolbar *toolbar);
void bloom_toolbar_toggle_buttons       (BloomToolbar *toolbar, const gchar *name, gboolean activate);
void bloom_toolbar_unactive_applets     (BloomToolbar  *toolbar, const gchar *name);
void bloom_toolbar_resize_panels        (BloomToolbar  *toolbar, int screen_width, int screen_height);
void bloom_toolbar_move_applets         (BloomToolbar  *toolbar, int screen_width);

G_END_DECLS

#endif /* _BLOOM_TOOLBAR */

