/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/* bloom-toolbar.c */
/*
 * Copyright (c) 2009, 2010 Agorabox.
 *
 * Authors: Bastien Bouzerau <bastien.bouzerau@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ccss/ccss.h>
#include "config.h"

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <dbus/dbus.h>
#include <gconf/gconf-client.h>
#include <moblin-panel/mpl-panel-common.h>

#include "bloom-toolbar.h"

#define KEY_DIR "/desktop/bloom/toolbar/panels"
#define KEY_ORDER KEY_DIR "/order"
#define PANELSDIR "/usr/share/bloom/panels"

G_DEFINE_TYPE (BloomToolbar, bloom_toolbar, G_TYPE_OBJECT)

#define BLOOM_TOOLBAR_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), BLOOM_TYPE_TOOLBAR, BloomToolbarPrivate))

static void bloom_toolbar_constructed (GObject *self);
static void bloom_toolbar_handle_dbus_name (BloomToolbar *, const gchar *);
static ToolbarPendingPanel *bloom_toolbar_make_pending_from_desktop (BloomToolbar *toolbar, const gchar *desktop);
static void bloom_toolbar_append_panel (BloomToolbar  *toolbar, BloomPanel *panel);
static ToolbarPendingPanel* bloom_toolbar_get_pending_panel (BloomToolbar *toolbar, const gchar *name);
static void mnb_toolbar_dbus_show_toolbar (BloomToolbar *self);
static void mnb_toolbar_dbus_hide_toolbar (BloomToolbar *self);
static void mnb_toolbar_dbus_show_panel (BloomToolbar *self, const char *name);
static void mnb_toolbar_dbus_hide_panel (BloomToolbar *self, const char *name, gboolean hide_toolbar);

static ccss_function_t const * peek_css_functions (void);

guint signals_tb[LAST_SIGNAL] = { 0 };

struct _BloomToolbarPrivate
{
    DBusGConnection *dbus_conn;
    DBusGProxy      *dbus_proxy;
    BloomPanel      *panels[NUM_ZONES];
    uint             crash_count[NUM_ZONES];
    uint             first_crash_time[NUM_ZONES];
    GConfClient     *gconf_client;
    GSList          *pending_panels;
    gchar           *name;
    GdkWindow       *window;
};

typedef struct 
{
    ccss_node_t  parent;
    char const  *type_name;
    char const  *id;
    char const  *pseudo_classes[2];
} Node;

BloomToolbar* bloom_toolbar_new ()
{
    return g_object_new (BLOOM_TYPE_TOOLBAR, NULL);
}

static GObject *bloom_toolbar_constructor (GType type, guint n_properties,
                                           GObjectConstructParam *properties)
{
    BloomToolbarClass *klass = NULL;

    klass = g_type_class_peek (BLOOM_TYPE_TOOLBAR);
    if (klass->instance == NULL){
        klass->instance = G_OBJECT_CLASS (bloom_toolbar_parent_class)->constructor (type,
                                                                            n_properties,
                                                                            properties);
    }
    else{
        klass->instance = g_object_ref (klass->instance);
    }
    return klass->instance;
}

static void bloom_toolbar_finalize (GObject *self)
{
    BloomToolbarClass *klass = NULL;

    klass = g_type_class_peek (BLOOM_TYPE_TOOLBAR);
    klass->instance = NULL;
    G_OBJECT_CLASS (bloom_toolbar_parent_class)->finalize (self);
}

#include "moblin-panel/mnb-toolbar-dbus-glue.h"

static void bloom_toolbar_class_init (BloomToolbarClass *klass)
{
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

    g_type_class_add_private (klass, sizeof (BloomToolbarPrivate));

    gobject_class->constructor = bloom_toolbar_constructor;
    gobject_class->finalize = bloom_toolbar_finalize;
    gobject_class->constructed = bloom_toolbar_constructed;

    dbus_g_object_type_install_info (G_TYPE_FROM_CLASS (klass),
                                     &dbus_glib_mnb_toolbar_dbus_object_info);

    signals_tb[REQUEST_BUTTON_STYLE] =
    g_signal_new ("request-button-style",
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BloomToolbarClass, request_button_style),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,
                  G_TYPE_POINTER);

    signals_tb[REQUEST_TOOLTIP] =
    g_signal_new ("request-tooltip",
                  G_TYPE_FROM_CLASS (gobject_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (BloomToolbarClass, request_tooltip),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,
                  G_TYPE_POINTER);

    signals_tb[REQUEST_UNACTIVE_BUTTONS] =
    g_signal_new ("request-unactive-buttons",
                   G_TYPE_FROM_CLASS (gobject_class),
                   G_SIGNAL_RUN_LAST,
                   G_STRUCT_OFFSET (BloomToolbarClass, request_unactive_buttons),
                   NULL, NULL,
                   g_cclosure_marshal_VOID__STRING,
                   G_TYPE_NONE, 1,
                   G_TYPE_STRING);

    signals_tb[REQUEST_ACTIVE_BUTTONS] =
    g_signal_new ("request-active-buttons",
                   G_TYPE_FROM_CLASS (gobject_class),
                   G_SIGNAL_RUN_LAST,
                   G_STRUCT_OFFSET (BloomToolbarClass, request_active_buttons),
                   NULL, NULL,
                   g_cclosure_marshal_VOID__STRING,
                   G_TYPE_NONE, 1,
                   G_TYPE_STRING);

    signals_tb[CLICKED] =
    g_signal_new ("clicked",
                   G_TYPE_FROM_CLASS (gobject_class),
                   G_SIGNAL_RUN_LAST,
                   G_STRUCT_OFFSET (BloomToolbarClass, clicked),
                   NULL, NULL,
                   g_cclosure_marshal_VOID__STRING,
                   G_TYPE_NONE, 1,
                   G_TYPE_STRING);

    signals_tb[REMOTE_PROCESS_DIED] =
    g_signal_new ("remote-process-died",
                G_TYPE_FROM_CLASS (gobject_class),
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET (BloomToolbarClass, remote_process_died),
                NULL, NULL,
                g_cclosure_marshal_VOID__STRING,
                G_TYPE_NONE, 1,
                G_TYPE_STRING);

    klass->instance = NULL;
}

static void bloom_toolbar_init (BloomToolbar *self)
{
    BloomToolbarPrivate *priv;
    priv = self->priv = BLOOM_TOOLBAR_GET_PRIVATE (self);
    self->name = "bloom";
    g_return_if_fail (self != NULL);

}

static char *
ccss_url (GSList const  *args,
          void          *user_data)
{
    const gchar *given_path, *filename;
    gchar *test_path;

    g_return_val_if_fail (args, NULL);

    given_path = (char const *) args->data;

    /* we can only deal with local paths */
    if (!g_str_has_prefix (given_path, "file://"))
        return NULL;
    filename = &given_path[7];

    /*
    * Handle absolute paths correctly
    */
    if (*filename == '/')
        return strdup (filename);

    /* first try looking in the theme dir */
    test_path = g_build_filename (g_get_user_config_dir (),
                                "nbtk",
                                filename,
                                NULL);
    if (g_file_test (test_path, G_FILE_TEST_IS_REGULAR))
        return test_path;
    g_free (test_path);

    if (user_data) {
        test_path = g_build_filename ((gchar *) user_data, filename, NULL);

        if (g_file_test (test_path, G_FILE_TEST_IS_REGULAR))
            return test_path;

        g_free (test_path);
    }
    else{
        g_warning ("No path available css url resolver!");
    }

    /* couldn't find the image anywhere, so just return the filename */
    return strdup (given_path);
}


static ccss_function_t const *
peek_css_functions (void)
{
    static ccss_function_t const ccss_functions[] =
    {
        { "url", ccss_url },
        { NULL }
    };

    return ccss_functions;
}

const gchar *
css_style_load_from_file (const gchar  *style_id,
                          const gchar  *filename,
                          GError      **error)
{
    ccss_grammar_t    *grammar;
    ccss_stylesheet_t *stylesheet;
    ccss_style_t      *style;
    gchar             *path;

    path = g_path_get_dirname (filename);
    grammar = ccss_grammar_create_css ();
    ccss_grammar_add_functions (grammar, peek_css_functions ());
    stylesheet = ccss_grammar_create_stylesheet_from_file (grammar,filename, path);
    ccss_grammar_destroy (grammar);
    if ((style = ccss_stylesheet_query_type (stylesheet, style_id))) {
        gchar *string = NULL;
        ccss_style_get_string (style, "background-image", &string);
        return string;
    } else {
        return "";
    }
}

void
bloom_toolbar_toggle_buttons (BloomToolbar  *toolbar, const gchar *name, gboolean activate)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i = 0;
    gint index = bloom_toolbar_panel_name_to_index (name);
    gboolean is_applet = index >= APPLETS_START;
    gint screen_width, screen_height;
    GdkScreen *screen = gdk_screen_get_default ();
    ToolbarPendingPanel *pending;

    screen_width  = gdk_screen_get_width (screen);
    screen_height  = gdk_screen_get_height (screen);

    pending = bloom_toolbar_get_pending_panel(toolbar, name);

    /*
     * Spawn a new instance of panel
     */
    if (priv->panels[index] == NULL && pending && activate) {
        pending->request_show = TRUE;
        bloom_panel_ping (priv->dbus_conn, pending->service);
    }

    if (is_applet)
    {
        i = APPLETS_START;

        for (; i < NUM_ZONES; i++) {
            if (i == index && priv->panels[i] != NULL &&
                (!bloom_panel_is_shown(priv->panels[i]) || activate)) {
                bloom_panel_request_show_cb (priv->panels[i]);
            } else {
                if (priv->panels[i] != NULL) {
                    bloom_panel_request_hide_cb (priv->panels[i]);
                } 
            }
        }
    }
    else {
        for (i = NUM_ZONES - 1; i >= 0; i--) {
            if (priv->panels[i] != NULL) {
                if (i == index) {
                    if (activate) {
                        bloom_panel_request_show_cb (priv->panels[i]);
                    }
                    else {
                        bloom_panel_request_hide_cb (priv->panels[i]);
                    }
                }
            }
        }

        if (pending && activate) {
            if (!pending->show_loading && !is_applet && index != MYZONE) {
                g_debug ("Creating loading panel for panel %s", name);
                GdkColor white = {0,0xffff,0xffff,0xffff};

                GtkWindow *window = pending->show_loading = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));
                gtk_window_set_type_hint (GTK_WINDOW (window), GDK_WINDOW_TYPE_HINT_DOCK);

                gtk_window_set_decorated (window, FALSE);

                gtk_widget_modify_bg (GTK_WIDGET (window), GTK_STATE_NORMAL, &white);

                GtkImage *image = GTK_IMAGE (gtk_image_new_from_file (THEMEDIR"/load.gif"));

                gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (image));

                gtk_widget_realize (GTK_WIDGET (window));

                gdk_window_maximize (GTK_WIDGET (window)->window);
                gtk_window_maximize (window);

                gdk_window_set_keep_above (GTK_WIDGET (window)->window, TRUE);
            }

            if (pending->show_loading) {
                gtk_widget_show_all (GTK_WIDGET (pending->show_loading));
                gtk_widget_show_now (GTK_WIDGET (pending->show_loading));
            }
        }
        else if (pending) {
            pending->request_show = FALSE;
            if (pending->show_loading) {
                gtk_widget_hide (GTK_WIDGET (pending->show_loading));
            }
        }

        for (i = NUM_ZONES - 1; i >= 0; i--) {
            if (priv->panels[i] != NULL) {
                if (i != bloom_toolbar_panel_name_to_index (name) &&
                    bloom_panel_is_shown (priv->panels[i]) && !activate) {
                        bloom_panel_request_hide_cb (priv->panels[i]);
                }
            }
        }
    }
}

void
bloom_toolbar_unactive_applets (BloomToolbar  *toolbar, const gchar *name)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i ;

    for (i = APPLETS_START; i < NUM_ZONES; i++) {
        if ((i == bloom_toolbar_panel_name_to_index (name)) &&
            priv->panels[i] != NULL && bloom_panel_is_shown(priv->panels[i])) {
            bloom_panel_request_hide_cb (priv->panels[i]);
        }
    }
}

GSList *
bloom_toolbar_get_pids (BloomToolbar  *toolbar)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i ;

    GSList *pid_panels = NULL;
    
    for (i = 0; i < NUM_ZONES; i++) {
        if (priv->panels[i] != NULL) {
            pid_panels = g_slist_append (pid_panels, (gpointer) bloom_panel_get_pid(priv->panels[i]));
        }
    }
    
    return pid_panels;
}

void
bloom_toolbar_resize_panels (BloomToolbar  *toolbar, int screen_width, int screen_height)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i ;

    for (i = 0; i < APPLETS_START; i++) {
        if (priv->panels[i] != NULL) {
            bloom_panel_resize (priv->panels[i], screen_width, screen_height);
        }
    }
}

void
bloom_toolbar_move_applets(BloomToolbar  *toolbar, int screen_width)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i ;

    for (i = APPLETS_START; i < NUM_ZONES; i++) {
        if (priv->panels[i] != NULL) {
            bloom_panel_ensure_position (priv->panels[i]);
        }
    }
}

static void
bloom_toolbar_panel_request_button_style_cb (BloomPanel    *panel,
                                             const gchar *style_id,
                                             BloomToolbar  *toolbar)
{
    const gchar *stylesheet = NULL;
    const gchar *css_property = NULL;
    const gchar *css_property_active = NULL;
    const gchar       *style_id_active;
    name_and_style nas;
    GError    *error = NULL;

    if (BLOOM_IS_PANEL (panel))
        stylesheet = bloom_panel_get_stylesheet (BLOOM_PANEL (panel));

    css_property = css_style_load_from_file (style_id, stylesheet, &error);
    nas.style = css_property;

    style_id_active = g_strconcat(style_id, "-active", NULL);
    css_property_active = css_style_load_from_file (style_id_active, stylesheet, &error);
    nas.style_active = css_property_active;

    nas.name = bloom_panel_get_name(panel);
    g_signal_emit (toolbar, signals_tb[REQUEST_BUTTON_STYLE], 0, &nas);
}

static void
bloom_toolbar_panel_request_tooltip_cb (BloomPanel    *panel,
                                        const gchar   *tooltip,
                                        BloomToolbar  *toolbar)
{
    name_and_style nas;
    nas.style = tooltip;
    nas.name = bloom_panel_get_name (panel);
    g_signal_emit (toolbar, signals_tb[REQUEST_TOOLTIP], 0, &nas);
}

static void
bloom_toolbar_request_unactive_buttons (BloomPanel    *panel,
                                        const gchar   *name,
                                        BloomToolbar  *toolbar)
{
    g_signal_emit (toolbar, signals_tb[REQUEST_UNACTIVE_BUTTONS], 0, name);
}

static void
bloom_toolbar_request_active_buttons   (BloomPanel    *panel,
                                        const gchar   *name,
                                        BloomToolbar  *toolbar)
{
    g_signal_emit (toolbar, signals_tb[REQUEST_ACTIVE_BUTTONS], 0, name);
}

static void
bloom_toolbar_remote_process_died (BloomPanel    *panel,
                                   const gchar   *name,
                                   BloomToolbar  *toolbar)
{
    int index = bloom_toolbar_panel_name_to_index (bloom_panel_get_name (panel));
    BloomToolbarPrivate *priv = toolbar->priv;
    priv->panels[index] = NULL;

    ToolbarPendingPanel* pending;
    pending = g_new0 (ToolbarPendingPanel, 1);
    pending->name = g_strdup (name);
    pending->service = g_strconcat (MPL_PANEL_DBUS_NAME_PREFIX, name, NULL);
    toolbar->pending_panels = g_slist_append (toolbar->pending_panels, pending);

    g_object_unref(panel);
    g_signal_emit (toolbar, signals_tb[REMOTE_PROCESS_DIED], 0, name);
}

static void
bloom_toolbar_append_panel (BloomToolbar  *toolbar, BloomPanel *panel)
{
  BloomToolbarPrivate *priv = toolbar->priv;
  gint               index;
  gchar             *button_style = NULL;
  const gchar       *name = NULL;
  const gchar       *tooltip = NULL;
  const gchar       *stylesheet = NULL;
  const gchar       *style_id = NULL;
  const gchar       *style_id_active = NULL;
  const gchar       *active = "-active";
  name_and_style    nas;

  if (BLOOM_IS_PANEL (panel))
    {
      name           = bloom_panel_get_name (BLOOM_PANEL (panel));
      tooltip        = bloom_panel_get_tooltip (BLOOM_PANEL (panel));
      stylesheet     = bloom_panel_get_stylesheet (BLOOM_PANEL (panel));
      style_id       = bloom_panel_get_button_style (BLOOM_PANEL (panel));
      g_strconcat(active, NULL);
      style_id_active = g_strconcat(style_id, active,NULL);
    }

  index = bloom_toolbar_panel_name_to_index (name);

  if (index < 0)
    return;

  g_signal_connect (panel, "request-unactive-buttons",
                    G_CALLBACK (bloom_toolbar_request_unactive_buttons),
                    toolbar);

  g_signal_connect (panel, "request-tooltip",
                    G_CALLBACK (bloom_toolbar_panel_request_tooltip_cb),
                    toolbar);

  g_signal_connect (panel, "request-active-buttons",
                    G_CALLBACK (bloom_toolbar_request_active_buttons),
                    toolbar);

  g_signal_connect (panel, "remote-process-died",
                    G_CALLBACK (bloom_toolbar_remote_process_died),
                    toolbar);

  /*
   * Create the button for this zone.
   */

  if (!style_id || !*style_id)
    button_style = g_strdup_printf ("%s-button", name);

  g_signal_connect (panel, "request-button-style",
                    G_CALLBACK (bloom_toolbar_panel_request_button_style_cb),
                    toolbar);

  if (stylesheet && *stylesheet)
    {
      nas.name = name;

      GError    *error = NULL;
      const gchar *css_style_button = NULL;
      css_style_button = css_style_load_from_file (style_id, stylesheet, &error);
      if (!g_strcmp0(css_style_button, ""))
        {
          if (error)
          {
            g_warning ("Unable to load stylesheet %s: %s",
                       stylesheet, error->message);
                       g_error_free (error);
          }
        }
      nas.style = css_style_button;

      error = NULL;
      css_style_button = css_style_load_from_file (style_id_active, stylesheet, &error);
      if (!g_strcmp0(css_style_button, ""))
        {
          if (error)
            {
              g_warning ("Unable to load stylesheet %s: %s",
                         stylesheet, error->message);
                         g_error_free (error);
            }
        }
      nas.style_active = css_style_button;
      g_signal_emit (toolbar, signals_tb[REQUEST_BUTTON_STYLE], 0, &nas);
    }

  // Panel already exists (crashed)
  if (priv->panels[index] != NULL)
    {
      time_t       now;
      struct tm    *ts;
      int          current_time;
      now = time(NULL);
      ts = localtime(&now);
      current_time = ts->tm_sec + ts->tm_min * 60 + ts->tm_hour * 60 * 60 + ts->tm_yday * 24 * 60 * 60;

      if (current_time - priv->first_crash_time[index] > 5 * 60)
        {
          priv->crash_count[index] = 1;
          priv->first_crash_time[index] = current_time;
        }
      else
        {
          priv->crash_count[index]++;
        }
    }

  priv->panels[index] = BLOOM_PANEL (panel);

  if (tooltip)
    {
      name_and_style nas;

      nas.style = tooltip;
      nas.name = bloom_panel_get_name(panel);
      g_signal_emit (toolbar, signals_tb[REQUEST_TOOLTIP], 0, &nas);
    }

  ToolbarPendingPanel *pending = bloom_toolbar_get_pending_panel (toolbar, name);
  if (pending)
    {
      if (index == MYZONE || pending->request_show || gtk_widget_get_visible (GTK_WIDGET (pending->show_loading)))
        {
          bloom_panel_request_show_cb (priv->panels[index]);
          return;
        }
    }
}

static DBusGConnection *
bloom_toolbar_connect_to_dbus (BloomToolbar *self)
{
  BloomToolbarPrivate *priv = self->priv;
  DBusGConnection   *conn;
  DBusGProxy        *proxy;
  GError            *error = NULL;
  guint              status;

  conn = dbus_g_bus_get (DBUS_BUS_SESSION, &error);

  if (!conn)
    {
      g_warning ("Cannot connect to DBus: %s", error->message);
      g_error_free (error);
      return NULL;
    }

  proxy = dbus_g_proxy_new_for_name (conn,
                                     DBUS_SERVICE_DBUS,
                                     DBUS_PATH_DBUS,
                                     DBUS_INTERFACE_DBUS);

  if (!proxy)
    {
      g_object_unref (conn);
      return NULL;
    }

  if (!org_freedesktop_DBus_request_name (proxy,
                                          MPL_TOOLBAR_DBUS_NAME,
                                          DBUS_NAME_FLAG_DO_NOT_QUEUE,
                                          &status, &error))
    {
      if (error)
        {
          g_warning ("%s: %s", __FUNCTION__, error->message);
          g_error_free (error);
        } else {
          g_warning ("%s: Unknown error", __FUNCTION__);
        }

      g_object_unref (conn);
      conn = NULL;
    }

  priv->dbus_proxy = proxy;

  dbus_g_proxy_add_signal (proxy, "NameOwnerChanged",
                           G_TYPE_STRING,
                           G_TYPE_STRING,
                           G_TYPE_STRING,
                           G_TYPE_INVALID);

  return conn;
}

static void
mnb_toolbar_noc_cb (DBusGProxy  *proxy,
                    const gchar *name,
                    const gchar *old_owner,
                    const gchar *new_owner,
                    BloomToolbar  *toolbar)
{
  BloomToolbarPrivate *priv;
  GSList            *l;

  /*
   * Unfortunately, we get this for all name owner changes on the bus, so
   * return early.
   */
  if (!name || strncmp (name, MPL_PANEL_DBUS_NAME_PREFIX,
                        strlen (MPL_PANEL_DBUS_NAME_PREFIX)))
    return;

  g_debug ("mnb_toolbar_noc_cb %s %s %s", name, old_owner, new_owner);

  priv = BLOOM_TOOLBAR (toolbar)->priv;

  if (!new_owner || !*new_owner)
    {
      /*
       * This is the case where a panel gone away; we can ignore it here,
       * as this gets handled nicely elsewhere.
       */
      return;
    }

  l = priv->pending_panels;
  while (l)
    {
      const gchar *my_name = l->data;

      if (!strcmp (my_name, name))
        {
          /* We are already handling this one */
          return;
        }

      l = l->next;
    }

  bloom_toolbar_handle_dbus_name (toolbar, name);
}

static ToolbarPendingPanel*
bloom_toolbar_get_pending_panel (BloomToolbar *toolbar, const gchar *name)
{
  GSList            *l;
  l = toolbar->pending_panels;
  while (l)
    {
      ToolbarPendingPanel *pending = l->data;

      if (!g_strcmp0 (g_strrstr (pending->service, ".") + 1, name))
        {
          return pending;
        }

      l = l->next;
    }
  return NULL;
}

static void
bloom_toolbar_panel_show_complete (BloomToolbar  *toolbar, BloomPanel *panel)
{
  int i;
  int index;
  GSList *l;

  /*
   * Remove this panel from the pending list.
   */
  const gchar *name = bloom_panel_get_name (panel);
  ToolbarPendingPanel *pending = bloom_toolbar_get_pending_panel (toolbar, name);

  bloom_toolbar_request_active_buttons (panel, name, toolbar);

  if (pending && pending->show_loading)
    {
      gtk_widget_destroy (GTK_WIDGET (pending->show_loading));
      pending->show_loading = NULL;
      toolbar->pending_panels = g_slist_delete_link (toolbar->pending_panels,
                                                     g_slist_find (toolbar->pending_panels, pending));
    }

  index = bloom_toolbar_panel_name_to_index (name);
  if (index < APPLETS_START)
    {
      for (i = 0; i < NUM_ZONES; i++)
        {
          if (i != index && toolbar->priv->panels[i] && bloom_panel_is_shown (toolbar->priv->panels[i]))
            {
              bloom_panel_request_hide_cb (toolbar->priv->panels[i]);
            }
        }
    }

  l = toolbar->pending_panels;
  while (l)
    {
      ToolbarPendingPanel *pending = l->data;
      pending->request_show = FALSE;
      if (pending->show_loading)
        {
          gtk_widget_hide (GTK_WIDGET (pending->show_loading));
        }
      l = l->next;
    }
}

static void
bloom_toolbar_handle_dbus_name (BloomToolbar *toolbar, const gchar *name)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    BloomPanel *panel;
    gint screen_width, screen_height;
    GdkScreen *screen;

    const gchar  *short_name = name + strlen (MPL_PANEL_DBUS_NAME_PREFIX);
    if (!strcmp (short_name, MPL_PANEL_MYZONE) ||
        !strcmp (short_name, MPL_PANEL_STATUS) ||
        !strcmp (short_name, MPL_PANEL_PASTEBOARD) ||
        !strcmp (short_name, MPL_PANEL_PEOPLE) ||
        !strcmp (short_name, MPL_PANEL_MEDIA) ||
        !strcmp (short_name, MPL_PANEL_INTERNET) ||
        !strcmp (short_name, MPL_PANEL_APPLICATIONS) ||
        !strcmp (short_name, MPL_PANEL_DEVICE) ||
        !strcmp (short_name, MPL_PANEL_MAIL) ||
        !strcmp (short_name, MPL_PANEL_POWER) ||
        !strcmp (short_name, MPL_PANEL_SYNC) ||
        !strcmp (short_name, MPL_PANEL_NETWORK) ||
        !strcmp (short_name, MPL_PANEL_BLUETOOTH) ||
        !strcmp (short_name, MPL_PANEL_VOLUME) ||
        !strcmp (short_name, MPL_PANEL_TOGGLE) ||
        !strcmp (short_name, MPL_PANEL_SHUTDOWN) ||
        !strcmp (short_name, MPL_PANEL_TEST))
    {
        screen = gdk_screen_get_default ();
        screen_width  = gdk_screen_get_width (screen);
        screen_height  = gdk_screen_get_height (screen);

        if (priv->crash_count[bloom_toolbar_panel_name_to_index (short_name)] < 5) {
            panel  = bloom_panel_new (name,
                                      screen_width,
                                      screen_height - TOOLBAR_HEIGHT, true);
        } else {
            priv->crash_count[bloom_toolbar_panel_name_to_index (short_name)] = 0;
            panel  = bloom_panel_new (name,
                                      screen_width,
                                      screen_height - TOOLBAR_HEIGHT, false);
        }

        if (panel) {
            if (bloom_panel_is_ready (panel)) {
                bloom_toolbar_append_panel (toolbar, panel);
            } else {
                g_signal_connect_swapped (panel, "ready",
                                          G_CALLBACK (bloom_toolbar_append_panel),
                                          toolbar);

                g_signal_connect_swapped (panel, "show-complete",
                                          G_CALLBACK (bloom_toolbar_panel_show_complete),
                                          toolbar);
            }
        }
    }
}

gint
bloom_toolbar_panel_name_to_index (const gchar *name)
{
    gint index;

    if (!strcmp (name, MPL_PANEL_MYZONE))
    index = MYZONE;
    else if (!strcmp (name, MPL_PANEL_INTERNET))
    index = INTERNET_ZONE;
    else if (!strcmp (name, MPL_PANEL_MEDIA))
    index = MEDIA_ZONE;
    else if (!strcmp (name, MPL_PANEL_APPLICATIONS))
    index = APPS_ZONE;
    else if (!strcmp (name, MPL_PANEL_DEVICE))
    index = DEVICE_ZONE;
    else if (!strcmp (name, MPL_PANEL_MAIL))
    index = MAIL_ZONE;
    else if (!strcmp (name, MPL_PANEL_SYNC))
    index = SYNC_APPLET;
    else if (!strcmp (name, MPL_PANEL_NETWORK))
    index = WIFI_APPLET;
    else if (!strcmp (name, MPL_PANEL_BLUETOOTH))
    index = BT_APPLET;
    else if (!strcmp (name, MPL_PANEL_VOLUME))
    index = VOLUME_APPLET;
    else if (!strcmp (name, MPL_PANEL_POWER))
    index = BATTERY_APPLET;
    else if (!strcmp (name, MPL_PANEL_TOGGLE))
    index = TOGGLE_APPLET;
    else if (!strcmp (name, MPL_PANEL_SHUTDOWN))
    index = SHUTDOWN_APPLET;
    else{
        g_warning ("Unknown panel [%s]", name);
        index = -1;
    }

    return index;
}

static void
bloom_toolbar_dbus_setup_panels (BloomToolbar *toolbar)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gchar             **names = NULL;
    GError             *error = NULL;
    gboolean            found_panels[NUM_ZONES];

    int i;
    for (i = 0; i < NUM_ZONES; i++) {
        priv->crash_count[i] = 0;
        priv->first_crash_time[i] = 0;
    }

    if (!priv->dbus_conn || !priv->dbus_proxy){
        g_warning ("DBus connection not available, cannot start panels !!!");
        return;
    }

    memset (&found_panels, 0, sizeof(found_panels));
    if (org_freedesktop_DBus_list_names (priv->dbus_proxy, &names, &error)) {
        gchar **p = names;
        while (*p) {
            if (!strncmp (*p, MPL_PANEL_DBUS_NAME_PREFIX,
                          strlen (MPL_PANEL_DBUS_NAME_PREFIX))) {
                gboolean has_owner = FALSE;
                if (org_freedesktop_DBus_name_has_owner (priv->dbus_proxy,
                                                         *p, &has_owner, NULL) && has_owner) {
                    const gchar *short_name;
                    gint         index;

                    short_name = *p + strlen (MPL_PANEL_DBUS_NAME_PREFIX);

                    index = bloom_toolbar_panel_name_to_index (short_name);

                    if (index >= 0)
                        found_panels[index] = TRUE;

                    g_debug ("Found dbus name %s", *p);

                    bloom_toolbar_handle_dbus_name (toolbar, *p);
                }
            }
            p++;
        }
    }

    dbus_free_string_array (names);

    dbus_g_proxy_connect_signal (priv->dbus_proxy, "NameOwnerChanged",
                                 G_CALLBACK (mnb_toolbar_noc_cb),
                                 toolbar, NULL);
}

void bloom_toolbar_hide_all_panels (BloomToolbar *toolbar)
{
    BloomToolbarPrivate *priv = toolbar->priv;
    gint i = 0;
    for (; i < NUM_ZONES; i++) {
        if (priv->panels[i] != NULL) {
            bloom_panel_request_hide_cb (priv->panels[i]);
        }
    }
}

/*
 * Creates MnbToolbarPanel object from a desktop file.
 *
 * desktop is the desktop file name without the .desktop prefix, which will be
 * loaded from standard location.
 */
static ToolbarPendingPanel *
bloom_toolbar_make_pending_from_desktop (BloomToolbar *toolbar, const gchar *desktop)
{
  gchar           *path;
  GKeyFile        *kfile;
  GError          *error = NULL;
  ToolbarPendingPanel *tp = NULL;

  path = g_strconcat (PANELSDIR, "/", desktop, ".desktop", NULL);

  kfile = g_key_file_new ();

  tp = g_new0 (ToolbarPendingPanel, 1);

  tp->name = g_strdup (desktop);

  if (!g_key_file_load_from_file (kfile, path, G_KEY_FILE_NONE, &error))
    {
      g_warning ("Failed to load %s: %s", path, error->message);
      g_clear_error (&error);
    }
  else
    {
      gchar    *s;
      gboolean  b;
      gboolean  builtin = FALSE;

      b = g_key_file_get_boolean (kfile,
                                  G_KEY_FILE_DESKTOP_GROUP,
                                  "X-Meego-Panel-Optional",
                                  &error);

      /*
       * If the key does not exist (i.e., there is an error), the panel is
       * not required
       */
      if (!error)
        tp->required = !b;
      else
        g_clear_error (&error);

      b = g_key_file_get_boolean (kfile,
                                  G_KEY_FILE_DESKTOP_GROUP,
                                  "X-Meego-Panel-Windowless",
                                  &error);

      /*
       * If the key does not exist (i.e., there is an error), the panel is
       * not windowless
       */
      if (!error)
        tp->windowless = b;
      else
        g_clear_error (&error);

      s = g_key_file_get_locale_string (kfile,
                                        G_KEY_FILE_DESKTOP_GROUP,
                                        G_KEY_FILE_DESKTOP_KEY_NAME,
                                        NULL,
                                        NULL);

      tp->tooltip = s;

      s = g_key_file_get_string (kfile,
                                 G_KEY_FILE_DESKTOP_GROUP,
                                 "X-Meego-Panel-Button-Style",
                                        NULL);

      if (!s)
        {
          /*
           * FIXME -- temporary hack
           */
          if (!strcmp (desktop, "meego-panel-myzone"))
            tp->button_style = g_strdup ("myzone-button");
          else
            tp->button_style = g_strdup_printf ("%s-button", desktop);
        }
      else
        tp->button_style = s;

      tp->active_button_style = g_strconcat (tp->button_style, "-active", NULL);

      s = g_key_file_get_string (kfile,
                                 G_KEY_FILE_DESKTOP_GROUP,
                                 "X-Meego-Panel-Type",
                                 NULL);

      if (s)
        {
          if (!strcmp (s, "applet"))
            tp->type = MNB_TOOLBAR_PANEL_APPLET;
          else if (!strcmp (s, "clock"))
            tp->type = MNB_TOOLBAR_PANEL_CLOCK;

          g_free (s);
        }

      if (!builtin)
        {
          s = g_key_file_get_string (kfile,
                                     G_KEY_FILE_DESKTOP_GROUP,
                                     "X-Meego-Panel-Stylesheet",
                                     NULL);

          tp->button_stylesheet = s;

          s = g_key_file_get_string (kfile,
                                     G_KEY_FILE_DESKTOP_GROUP,
                                     "X-Meego-Service",
                                     NULL);

          tp->service = s;
        }

      /*
       * Sanity check
       */
      /*
      if (!builtin && !tp->service)
        {
          g_warning ("Panel desktop file %s did not contain dbus service",
                     desktop);

          mnb_toolbar_panel_destroy (tp);
          tp = NULL;
        }
      */
    }

  g_key_file_free (kfile);

  return tp;
}

void bloom_toolbar_create_panels (BloomToolbar *toolbar, GSList *order)
{
  GSList *l;
  const gchar *name;
  ToolbarPendingPanel *pending;
  int index;

  for (l = order; l; l = l->next)
    {
      name = l->data;
      pending = bloom_toolbar_make_pending_from_desktop (toolbar, name);

      if (pending)
        {
          index = bloom_toolbar_panel_name_to_index (pending->service + strlen (MPL_PANEL_DBUS_NAME_PREFIX));

          pending->button_style = css_style_load_from_file (pending->button_style,
                                                            pending->button_stylesheet, NULL);
          pending->active_button_style = css_style_load_from_file (pending->active_button_style,
                                                                   pending->button_stylesheet, NULL);

          toolbar->pending_panels = g_slist_append (toolbar->pending_panels, pending);
        }
    }
}

void bloom_toolbar_setup_panels (BloomToolbar *toolbar)
{
  GSList *order;

  bloom_toolbar_dbus_setup_panels (toolbar);

  order = gconf_client_get_list (toolbar->priv->gconf_client, KEY_ORDER, GCONF_VALUE_STRING, NULL);

  bloom_toolbar_create_panels (toolbar, order);

  g_slist_foreach (order, (GFunc)g_free, NULL);
  g_slist_free (order);
}

static void bloom_toolbar_constructed (GObject *self)
{
    BloomToolbarPrivate *priv = NULL;
    DBusGConnection   *conn;

    priv = BLOOM_TOOLBAR_GET_PRIVATE (self);

    priv->gconf_client = gconf_client_get_default ();

    /*
    * Make sure our parent gets chance to do what it needs to.
    */
    if (G_OBJECT_CLASS (bloom_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (bloom_toolbar_parent_class)->constructed (self);

    if ((conn = bloom_toolbar_connect_to_dbus (BLOOM_TOOLBAR (self)))){
        priv->dbus_conn = conn;
        dbus_g_connection_register_g_object (conn, MPL_TOOLBAR_DBUS_PATH, self);
    } else {
        g_warning (G_STRLOC " DBus connection not available !!!");
    }

    bloom_toolbar_setup_panels (BLOOM_TOOLBAR (self));
}

static void mnb_toolbar_dbus_show_toolbar (BloomToolbar *self)
{
    bloom_toolbar_show ();
}

static void mnb_toolbar_dbus_hide_toolbar (BloomToolbar *self)
{
    bloom_toolbar_hide ();
}

static void mnb_toolbar_dbus_show_panel (BloomToolbar *self, const char *name)
{
    g_signal_emit (self, signals_tb[CLICKED], 0, name, name);
}

static void mnb_toolbar_dbus_hide_panel (BloomToolbar *self, const char *name, gboolean hide_toolbar)
{
}

