#include "qt-toolbar.h"
#include "bloom-main-button.h"

#include <iostream>

#include <glib-object.h>
#include <glib.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include <QDateTime>
#include <QPropertyAnimation>

Toolbar* Toolbar::instance = NULL;

Toolbar::Toolbar(QWidget *parent)
    : QWidget(parent, Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint)
{
    Toolbar::instance = this;

    int              i = 0;
    GSList          *panels;
    QSpacerItem     *spacer;
    QSpacerItem     *spacer2;
    QSpacerItem     *spacer3;
    int              screen_width;

    setObjectName ("toolbar");

    for (; i < NUM_ZONES; i++) {
        table_panel[i] = NULL;
    }

    QDesktopWidget *desktop = QApplication::desktop ();
    QRect screen_rect = desktop->screenGeometry (desktop->primaryScreen ());

    QSizePolicy sizePolicy (QSizePolicy::Expanding, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch (0);
    sizePolicy.setVerticalStretch (0);
    sizePolicy.setHeightForWidth (sizePolicy.hasHeightForWidth ());
    setSizePolicy (sizePolicy);

    setGeometry (0, 0, screen_width, TOOLBAR_HEIGHT);
    connect(desktop, SIGNAL (workAreaResized (int)), this, SLOT (screen_resize (int)));

    background = new QWidget (this);
    background->setObjectName ("background");
    background->setGeometry (0, 0, screen_width, TOOLBAR_HEIGHT);

    main_layout = new QHBoxLayout (this);
    main_layout->setContentsMargins (5, 0, 10, 0);
    main_layout->setSpacing (0);

    spacer = new QSpacerItem (30, 0, QSizePolicy::Maximum, QSizePolicy::Minimum);
    spacer2 = new QSpacerItem (0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    spacer3 = new QSpacerItem (30, 0, QSizePolicy::Maximum, QSizePolicy::Minimum);

    /*
     * Create the main button at the top left corner.
     */
    logo = new BloomMainButton (this);
    main_layout->addWidget (logo);
    main_layout->addItem (spacer);
    main_layout->addItem (spacer2);

    /*
     * Create the clock applet
     * Should probably be an external program...
     */
    clock = new BloomClock (this);
    main_layout->addItem (spacer3);
    main_layout->addWidget (clock);

    /*
     * Create the BloomToolbar GObject
     * and connect signals to 'extern "C"' functions
     * that use C++
     */
    toolbar = bloom_toolbar_new ();

    g_signal_connect (toolbar, "request-button-style",
            G_CALLBACK (request_button_style_type),
            this);
    g_signal_connect (toolbar, "request-tooltip",
            G_CALLBACK (request_tooltip),
            this);
    g_signal_connect (toolbar, "request-unactive-buttons",
            G_CALLBACK (request_unactive_buttons),
            this);
    g_signal_connect (toolbar, "request-active-buttons",
            G_CALLBACK (request_active_buttons),
            this);
    g_signal_connect (toolbar, "clicked",
            G_CALLBACK (request_button_clicked),
            this);

    logo->wm->flush ();

    for (panels = toolbar->pending_panels; panels; panels = panels->next) {
        bool button_exists = false;
        BloomButton *button;
        ToolbarPendingPanel *panel = (ToolbarPendingPanel*) panels->data;
        QString name = panel->service;
        name = name.mid (name.lastIndexOf(".") + 1);
        for (i = 0; i < list_panels.size (); ++i) {
            if (list_panels.at(i)->objectName() == name) {
                button_exists = true;
            }
        }
        if (!button_exists) {
            button = bloom_add_button (g_strrstr (panel->service, ".") + 1);
            button->set_pixmaps (QPixmap (panel->button_style),
                                 QPixmap (panel->active_button_style));
            QString tooltip = QString::fromUtf8 (panel->tooltip);
            tooltip[0] = tooltip[0].toUpper();
            button->set_tooltip (tooltip);
        }
     }

    /*
     * Set the toolbar as a 'strut' window so that
     * no other window can be over it
     */
    Display *display = GDK_DISPLAY_XDISPLAY (gdk_display_get_default());
    long vals [4] = { 0, 0, TOOLBAR_HEIGHT, 0};
    static Atom net_wm_window_type_dock = None;
    net_wm_window_type_dock = XInternAtom (display,
                                           "_NET_WM_WINDOW_TYPE_DOCK",
                                           False);

    XChangeProperty (display, winId(),
                     XInternAtom (display,"_NET_WM_WINDOW_TYPE", False),
                     XA_ATOM, 32, PropModeReplace,
                     (guchar *) &net_wm_window_type_dock, 1);

    XChangeProperty (display,
                     winId(),
                     XInternAtom (display, "_NET_WM_STRUT", False),
                     XA_CARDINAL, 32, PropModeReplace,
                     (unsigned char *) vals, 4);

    show_home_panel_if_needed ();
}

void Toolbar::bloom_toolbar_hide_all ()
{
    bloom_toolbar_hide_all_panels (toolbar);

    for (int i = 0; i < NUM_ZONES; ++i){
        if (table_panel[i] != 0) {
            table_panel[i]->set_active (false);
        }
    }
}

BloomButton* Toolbar::bloom_add_button (const gchar *short_name)
{
    int pos_on_toolbar;
    BloomButton *button;
    int index = bloom_toolbar_panel_name_to_index (short_name);
    bool is_applet = index > APPLETS_START;

    if (is_applet) {
        pos_on_toolbar = main_layout->indexOf (clock) - 1;
        for (int i = 0; i < list_panels.size (); i++) {
            const char *panel_name = list_panels[i]->objectName ().toStdString ().c_str ();
            int panel_index = bloom_toolbar_panel_name_to_index (panel_name);
            if (panel_index >= APPLETS_START && index < panel_index)
                pos_on_toolbar--;
        }
    } else {
        pos_on_toolbar = 2;
        for (int i = 0; i < list_panels.size (); i++) {
            const char *panel_name = list_panels[i]->objectName ().toStdString ().c_str ();
            int panel_index = bloom_toolbar_panel_name_to_index (panel_name);
            if (panel_index < APPLETS_START && index > panel_index)
                pos_on_toolbar += 2;
        }
    }

    button = new BloomButton (this, QString(short_name), !is_applet);
    list_panels.append (button);
    table_panel[index] = button;
    main_layout->insertWidget (pos_on_toolbar, button);

    if (!is_applet) {
        pos_on_toolbar++;
        QLabel *separator = new QLabel (this);
        separator->setObjectName ("separator");
        separator->setMaximumWidth (2);
        list_separators.append (separator);
        main_layout->insertWidget (pos_on_toolbar, separator);
    }
    
    return button;
}

void Toolbar::bloom_toolbar_toggled_button (QObject *source)
{
    int i = 0;
    QString *sender_name = new QString (source->objectName ());
    BloomButton *button = (BloomButton*) source;
    gboolean activate = true;

    if (bloom_toolbar_panel_name_to_index (sender_name->toStdString ().c_str ()) >= APPLETS_START)
    {
        i = APPLETS_START;
    }

    for (; i < NUM_ZONES; ++i) {
        BloomButton *panel_button = table_panel[i];
        if (panel_button != NULL) {
            if (panel_button->objectName () != source->objectName() ||
               (panel_button->objectName () == source->objectName() && panel_button->active)) {
                panel_button->set_active (false);
                if (panel_button->objectName () == source->objectName ()) {
                    activate = false;
                }
            }
        }
    }

    if (activate) {
        button->set_active (true);
    }

    bloom_toolbar_toggle_buttons (toolbar, sender_name->toStdString ().c_str (), activate);

    delete sender_name;
}

void Toolbar::screen_resize (int size)
{
    GdkScreen *screen;
    screen = gdk_screen_get_default ();
    gint screen_width = gdk_screen_get_width (screen);
    gint screen_height = gdk_screen_get_height (screen);

    setGeometry (0, 0, screen_width, TOOLBAR_HEIGHT);
    background->setGeometry (0, 0, screen_width, TOOLBAR_HEIGHT);
    bloom_toolbar_resize_panels (toolbar, screen_width, screen_height);
    bloom_toolbar_move_applets (toolbar, screen_width);
}

void Toolbar::bloom_toolbar_unactive_button (const gchar *name)
{
    QString *panel_name = new QString (name);
    for (int i = 0; i < list_panels.size (); ++i) {
        if (list_panels[i]->objectName () == *panel_name) {
            list_panels[i]->set_active (false);
        }
    }

    bloom_toolbar_unactive_applets (toolbar, name);
}

void Toolbar::bloom_toolbar_active_button (const gchar *name)
{
    QString *panel_name = new QString (name);
    for (int i = 0; i < list_panels.size (); ++i) {
        if (list_panels[i]->objectName () == *panel_name) {
            list_panels[i]->set_active (true);
        }
    }
}

int Toolbar::current_time_msec ()
{
    QTime now = QTime::currentTime ();
    return now.hour () * 60 * 60 * 1000 +
           now.minute () * 60 * 1000 +
           now.second () * 1000 + now.msec ();
}

void Toolbar::showEvent (QShowEvent *)
{
    animate_logo ();
}

void Toolbar::animate_logo ()
{
    logo->animate();
}

GSList *Toolbar::get_pids_panels ()
{
    return bloom_toolbar_get_pids (toolbar);
}

void Toolbar::show_home_panel_if_needed ()
{
    int home_index = bloom_toolbar_panel_name_to_index (MPL_PANEL_MYZONE);

    // Show the 'Home' panel is there is no active panel
    for (int i = home_index + 1; i < NUM_ZONES; ++i) {
        if (table_panel[i] && table_panel[i]->active) {
            return;
        }
    }

    bloom_toolbar_toggle_buttons (toolbar, MPL_PANEL_MYZONE, true);
    bloom_toolbar_active_button (MPL_PANEL_MYZONE);
}

/*
   Theses functions exist to be called from C
   by the GObject toolbar
 */

void request_button_style_type (BloomToolbar *toolbar,
                                gpointer data,
                                Toolbar *tb)
{
    bool button_exist = false;
    name_and_style * nas = (name_and_style *) data;
    QString name = QString (nas->name);
    for (int i = 0; i < tb->list_panels.size (); ++i) {
        if (tb->list_panels.at (i)->objectName () == name) {
            button_exist = true;
            tb->list_panels[i]->set_pixmaps (QPixmap (nas->style),
                                             QPixmap (nas->style_active));
        }
    }

    if (!button_exist) {
        tb->bloom_add_button (name.toStdString ().c_str ());
        request_button_style_type (toolbar, data, tb);
    }
}

void request_tooltip (BloomToolbar *toolbar,
                      gpointer data,
                      Toolbar *tb)
{
    name_and_style * nas = (name_and_style *) data;
    QString *name = new QString (nas->name);
    for (int i = 0; i < tb->list_panels.size (); ++i) {
        if (tb->list_panels[i]->objectName () == *name) {
            QString tooltip = QString::fromUtf8 (nas->style);
            tooltip[0] = tooltip[0].toUpper();
            tb->list_panels[i]->set_tooltip (tooltip);
        }
    }
}

void request_unactive_buttons (BloomToolbar  *toolbar,
                               gchar *name,
                               Toolbar *tb)
{
    tb->bloom_toolbar_unactive_button (name);
}

void request_active_buttons (BloomToolbar *toolbar,
                             gchar *name,
                             Toolbar *tb)
{
    tb->bloom_toolbar_active_button (name);
}

void request_button_clicked (BloomToolbar *toolbar,
                             gchar *name,
                             Toolbar *tb)
{
    int index = bloom_toolbar_panel_name_to_index (name);
    if (index != -1 && tb->table_panel[index] && tb->table_panel[index]->active) {
        return;
    }

    tb->bloom_toolbar_toggled_button (tb->table_panel[index]);
}

void bloom_toolbar_show ()
{
    if (Toolbar::instance)
        Toolbar::instance->show ();
}

void bloom_toolbar_hide ()
{
    if (Toolbar::instance)
        Toolbar::instance->hide ();
}

