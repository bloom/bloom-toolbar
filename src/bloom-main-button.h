#ifndef HEADER_BLOOM_MAIN_BUTTON
#define HEADER_BLOOM_MAIN_BUTTON

#include <QtGui>

class WindowManager;
class Toolbar;

class BloomMainButton: public QLabel {
public:
    BloomMainButton (Toolbar *parent);

    void animate ();

    void mouseReleaseEvent (QMouseEvent * event);
    void leaveEvent (QEvent *event);

public:
    WindowManager *wm;

protected:
    QPropertyAnimation *animation;
    QMovie *movie;
};

#endif
