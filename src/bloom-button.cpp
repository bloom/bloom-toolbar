#include "bloom-button.h"
#include "bloom-tooltip.h"
#include "qt-toolbar.h"

#define APPLET_SIZE 40 
#define BUTTON_SIZE 80
#define FIRST_PANEL_BUTTON_OFFSET 170

#define RELEASE_ANIMATION_DURATION 200
#define RELEASE_ANIMATION_DEFORMATION 5

#define PRESS_ANIMATION_DURATION 5000
#define PRESS_ANIMATION_DEFORMATION 10

#define ENTER_ANIMATION_DURATION 1000
#define ENTER_ANIMATION_EASING 4
#define ENTER_ANIMATION_DEFORMATION 10

QRect deflate_rect (QRect rect, int x, int y)
{
    return QRect (rect.x () + x, rect.y () + y,
                  rect.width () - x * 2, rect.height () - y * 2);
}

BloomButton::BloomButton (QWidget *parent, QString name, bool panel)
    : QPushButton (parent)
{
    is_panel = panel;
    active = false;
    toolbar = (Toolbar*) parent;

    setObjectName (name);

    icon = new QLabel (this);
    icon->setAlignment (Qt::AlignCenter);
    icon->setObjectName ("panelicon");
    icon->setScaledContents (TRUE);

    icon_active = new QLabel (this);
    icon_active->setAlignment (Qt::AlignCenter);
    icon_active->setObjectName ("paneliconactive");
    icon_active->setScaledContents (TRUE);
    icon_active->hide ();

    reposition ();

    tooltip = new BloomTooltip (this);

    QPoint offset (0, panel ? 0 : TOOLBAR_HEIGHT - APPLET_SIZE);

    QEasingCurve easing (QEasingCurve::OutElastic);
    easing.setAmplitude (ENTER_ANIMATION_EASING);

    enter_animation = new QPropertyAnimation (icon, "geometry", this);
    enter_animation->setDuration (ENTER_ANIMATION_DURATION);
    enter_animation->setStartValue (deflate_rect (icon->rect (),
                                                  ENTER_ANIMATION_EASING,
                                                  ENTER_ANIMATION_EASING).translated (offset));
    enter_animation->setEndValue (icon->rect ().translated (offset));
    enter_animation->setEasingCurve (easing);

    pressed_animation = new QPropertyAnimation (icon, "geometry", this);
    pressed_animation->setDuration (PRESS_ANIMATION_DURATION);
    pressed_animation->setStartValue (icon->rect ().translated (offset));
    pressed_animation->setEndValue (deflate_rect (icon->rect (),
                                    PRESS_ANIMATION_DEFORMATION,
                                    PRESS_ANIMATION_DEFORMATION).translated (offset));

    released_animation = new QPropertyAnimation (icon, "geometry", this);
    released_animation->setDuration (RELEASE_ANIMATION_DURATION);
    released_animation->setStartValue (deflate_rect(icon->rect (),
                                       RELEASE_ANIMATION_DEFORMATION,
                                       RELEASE_ANIMATION_DEFORMATION).translated (offset));
    released_animation->setEndValue (icon->rect ().translated (offset));

    setFocusPolicy (Qt::NoFocus);

    released_animation->start ();
}

void BloomButton::enterEvent (QEvent *)
{
    if (!active) {
        reposition ();
        show_tooltip ();
        enter_animation->start ();
    }
}

void BloomButton::leaveEvent (QEvent *)
{
    reposition ();
    tooltip->hide ();
}

void BloomButton::mousePressEvent (QMouseEvent *event)
{
    tooltip->hide ();
    if (event->button() == Qt::LeftButton and !active) {
        pressed_animation->start ();
    }
}

void BloomButton::mouseReleaseEvent (QMouseEvent *event)
{
    if (event->x () <= width () && event->y () <= height ()) {
        if (event->button () == Qt::LeftButton) {
            released_animation->start ();
            toolbar->bloom_toolbar_toggled_button (this);
        }
        if (active) {
            tooltip->hide ();
        } else {
            show_tooltip ();
        }
    }
}

void BloomButton::show_tooltip ()
{
    tooltip->display ();
}

void BloomButton::set_tooltip (QString tooltip_label)
{
    tooltip->set_tooltip (tooltip_label);
}

void BloomButton::reposition ()
{
    if (is_panel) {
        setFixedSize (BUTTON_SIZE, TOOLBAR_HEIGHT);
        icon->setGeometry (0, 0, BUTTON_SIZE, TOOLBAR_HEIGHT);
        icon_active->setGeometry (0, 0, BUTTON_SIZE, TOOLBAR_HEIGHT);
    } else {
        setFixedSize (APPLET_SIZE, TOOLBAR_HEIGHT);
        icon->setGeometry (0, TOOLBAR_HEIGHT - APPLET_SIZE,
                           APPLET_SIZE, APPLET_SIZE);
        icon_active->setGeometry (0, TOOLBAR_HEIGHT - APPLET_SIZE,
                                  APPLET_SIZE, APPLET_SIZE);
    }
}

void BloomButton::set_active (bool state)
{
    if (!state) {
        icon_active->hide ();
        icon->show ();
    }
    else {
        icon->hide ();
        icon_active->show ();
    }
    active = state;
}

void BloomButton::set_pixmaps (QPixmap inactive, QPixmap active)
{
    icon->setPixmap (inactive);
    icon_active->setPixmap (active);
}

