#include "bloom-main-button.h"
#include "window-manager.h"

BloomMainButton::BloomMainButton (Toolbar *parent)
{
    // Create the window list widget
    wm = new WindowManager (parent);

    animation = new QPropertyAnimation (this, "geometry", this);
    animation->setDuration (700);
    animation->setStartValue (QRect (pos ().x (), pos ().y () - height (),
                                    width (), height ()));
    animation->setEndValue (QRect (pos ().x (), pos ().y (),
                           width (), height ()));

    /* We want to show only the first frame now */
    movie = new QMovie (THEMEDIR"/logo.gif");
    movie->setSpeed (100);
    movie->start ();
    movie->stop ();

    setMovie (movie);

    setMouseTracking (true);
}

void BloomMainButton::animate ()
{
    movie->start();
}

void BloomMainButton::mouseReleaseEvent(QMouseEvent * event)
{
    wm->display();
}

void BloomMainButton::leaveEvent (QEvent *event)
{
    wm->start_hide_timer ();
}
