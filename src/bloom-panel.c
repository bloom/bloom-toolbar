/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/* bloom-panel.c */
/*
 * Copyright (c) 2009, 2010 Agorabox.
 *
 * Authors: Bastien Bouzerau <bastien.bouzerau@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <string.h>

#include "bloom-panel.h"
#include "bloom-toolbar.h"
#include "marshal.h"
#include "moblin-panel/mnb-panel-dbus-bindings.h"

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <dbus/dbus.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-client.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#define PANEL_DEFAULT_TIMEOUT 300000

G_DEFINE_TYPE (BloomPanel, bloom_panel, G_TYPE_OBJECT)

#define BLOOM_PANEL_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), BLOOM_TYPE_PANEL, BloomPanelPrivate))

static void     bloom_panel_constructed (GObject *self);

enum
{
    PROP_0,
    PROP_DBUS_NAME,
    PROP_WIDTH,
    PROP_HEIGHT,
};

guint signals_p[LAST_SIGNAL] = { 0 };

struct _BloomPanelPrivate
{
    DBusGConnection *dbus_conn;
    DBusGProxy      *proxy;
    gchar           *dbus_name;

    gchar           *name;
    gchar           *tooltip;
    gchar           *stylesheet;
    gchar           *button_style_id;
    guint            xid;
    guint            pid;
    guint            child_xid;

    guint            width;
    guint            height;

    gboolean         constructed      : 1;
    gboolean         dead             : 1; /* Set when the remote  */
    gboolean         ready            : 1;
    gboolean         show             : TRUE;
    gboolean         auto_rerun       : TRUE;
};

static void
bloom_panel_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
    BloomPanelPrivate *priv = BLOOM_PANEL (object)->priv;

    switch (property_id){
    case PROP_DBUS_NAME:
                       g_value_set_string (value, priv->dbus_name);
                       break;
    case PROP_WIDTH:
                       g_value_set_uint (value, priv->width);
                       break;
    case PROP_HEIGHT:
                       g_value_set_uint (value, priv->height);
                       break;
    default:
                       G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bloom_panel_set_property (GObject *object, guint property_id,
                        const GValue *value, GParamSpec *pspec)
{
    BloomPanelPrivate *priv = BLOOM_PANEL (object)->priv;

    switch (property_id){
    case PROP_DBUS_NAME:
                        g_free (priv->dbus_name);
                        priv->dbus_name = g_value_dup_string (value);
                        break;
    case PROP_WIDTH:
                        priv->width = g_value_get_uint (value);
                        break;
    case PROP_HEIGHT:
                        priv->height = g_value_get_uint (value);
                        break;
    default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bloom_panel_request_button_style_cb (DBusGProxy  *proxy,
                                     const gchar *style_id,
                                     BloomPanel    *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    g_free (priv->button_style_id);
    priv->button_style_id = g_strdup (style_id);
    g_signal_emit (panel, signals_p[REQUEST_BUTTON_STYLE], 0, style_id);
}

static void
bloom_panel_request_tooltip_cb (DBusGProxy  *proxy,
                                const gchar *tooltip,
                                BloomPanel  *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    g_free (priv->tooltip);
    priv->tooltip = g_strdup (tooltip);
    g_signal_emit (panel, signals_p[REQUEST_TOOLTIP], 0, tooltip);
}

static void
bloom_panel_ping_cb (DBusGProxy *proxy, GError *error, gpointer data)
{
    g_debug ("Received 'pong' from %s %s", dbus_g_proxy_get_bus_name (proxy),
                                           dbus_g_proxy_get_path (proxy));
}

static void
bloom_panel_do_nothing_reply_cb (DBusGProxy *proxy, GError *error, gpointer data)
{
}

static void
bloom_panel_show_end_reply_cb (DBusGProxy *proxy, GError *error, gpointer data)
{
    g_signal_emit (BLOOM_PANEL (data), signals_p[SHOW_COMPLETE], 0);
}

static void
bloom_panel_hide_begin_reply_cb (DBusGProxy *proxy, GError *error, gpointer data)
{
}

static void
bloom_panel_set_size_reply_cb (DBusGProxy *proxy, GError *error, gpointer data)
{
}

void
bloom_panel_request_show_cb (BloomPanel *panel)
{
    bloom_panel_ensure_position (panel);

    /*
     * We call the 'show' methods asynchronously but we need
     * to be sure the window is displayed before hiding the active
     * panel to prevent a random toplevel window to be activated
     * by the window manager (metacity)
     * A panel must therefore have displayed it window before
     * return from the 'Show' method
     */

    org_agorabox_bloom_UX_Shell_Panel_show_begin_async (panel->priv->proxy,
                                                        bloom_panel_do_nothing_reply_cb, NULL);

    org_agorabox_bloom_UX_Shell_Panel_show_async (panel->priv->proxy,
                                                  bloom_panel_do_nothing_reply_cb, NULL);

    org_agorabox_bloom_UX_Shell_Panel_show_end_async (panel->priv->proxy,
                                                      bloom_panel_show_end_reply_cb, panel);

    BLOOM_PANEL (panel)->priv->show = TRUE;
}

void
bloom_panel_request_hide_cb (BloomPanel *panel)
{
    //(DBusGProxy *proxy, org_agorabox_bloom_UX_Shell_Panel_show_begin_reply callback, gpointer userdata)
    org_agorabox_bloom_UX_Shell_Panel_hide_async (panel->priv->proxy,
                                                  bloom_panel_hide_begin_reply_cb,
                                                  panel);

    panel->priv->show = FALSE;
}

void
bloom_panel_resize (BloomPanel *panel, int width, int height)
{
    //(DBusGProxy *proxy, const guint IN_max_window_width, const guint IN_max_window_height, org_agorabox_bloom_UX_Shell_Panel_set_size_reply callback, gpointer userdata)

    g_debug (G_STRLOC " Got size change on Panel %s to %dx%d",
        bloom_panel_get_name (panel), width, height);

    /*
     * Resize our top-level window to match.
     */

    org_agorabox_bloom_UX_Shell_Panel_set_size_async (panel->priv->proxy,
                                                      width,
                                                      height - TOOLBAR_HEIGHT,
                                                      bloom_panel_set_size_reply_cb,
                                                      panel);
}

void
bloom_panel_ensure_position (BloomPanel *panel)
{
    GdkScreen *screen = gdk_screen_get_default ();
    gint screen_width  = gdk_screen_get_width (screen);

    // If we are dealing with an applet
    if (bloom_toolbar_panel_name_to_index (panel->priv->name) >= APPLETS_START) {
        bloom_panel_move (panel, screen_width - panel->priv->width, TOOLBAR_HEIGHT);
    }
    else {
        bloom_panel_move (panel, 0, TOOLBAR_HEIGHT);
    }
}

void
bloom_panel_move (BloomPanel *panel, int x, int y)
{
    org_agorabox_bloom_UX_Shell_Panel_set_position (panel->priv->proxy,
                                                    x, y, NULL);
}

static void
bloom_panel_request_hide_cb_from_panel (DBusGProxy  *proxy,
                                        BloomPanel    *panel)
{
    // The panel "panel->priv->name" doesn't have the focus anymore. We will hide its button
    g_signal_emit (&(panel->parent), signals_p[REQUEST_UNACTIVE_BUTTONS], 0, panel->priv->name);
}

static void
bloom_panel_request_show_cb_from_panel (DBusGProxy  *proxy,
                                        BloomPanel    *panel)
{
    // The panel "panel->priv->name" have the focus. We will show its button
    g_signal_emit (&(panel->parent), signals_p[REQUEST_ACTIVE_BUTTONS], 0, panel->priv->name);
}

static void
bloom_panel_init_panel_reply_cb (DBusGProxy *proxy,
                                 gchar      *name,
                                 guint       xid,
                                 guint       pid,
                                 gchar      *tooltip,
                                 gchar      *stylesheet,
                                 gchar      *button_style_id,
                                 guint       window_width,
                                 guint       window_height,
                                 GError     *error,
                                 gpointer    panel)
{
    BloomPanelPrivate *priv = BLOOM_PANEL (panel)->priv;

    g_debug ("Panel %s was successfully initialized :", name);
    g_debug (" xid : %d", xid);
    g_debug (" pid : %d", pid);
    g_debug (" tooltip : %s", tooltip);
    g_debug (" stylesheet : %s", stylesheet);
    g_debug (" button_style_id : %s", button_style_id);
    g_debug (" window_width : %d", window_width);
    g_debug (" window_height : %d", window_height);

    if (error) {
        g_warning ("Could not initialize Panel %s: %s", name, error->message);
        return;
    }

    /*
    * We duplicate the return values, because we need to be able to replace them
    * and using the originals we would need to use dbus_malloc() later on
    * to set them afresh.
    */
    g_free (priv->name);
    priv->name = g_strdup (name);

    if (!priv->tooltip)
        priv->tooltip = g_strdup (tooltip);

    g_free (priv->stylesheet);
    priv->stylesheet = g_strdup (stylesheet);

    g_free (priv->button_style_id);
    priv->button_style_id = g_strdup (button_style_id);

    priv->child_xid = xid;
    priv->pid = pid;

    priv->width = window_width;
    priv->height = window_height;

    /*
    * If we already have a window, we are being called because the panel has
    * died on us; we simply destroy the window and start again. (It should be
    * possible to just destroy the socket and reuse the window (after unmapping
    * it, but this is simple and robust.)
    */

    priv->ready = TRUE;
    priv->dead = FALSE;
    
    dbus_free (name);
    dbus_free (tooltip);
    dbus_free (stylesheet);
    dbus_free (button_style_id);

    g_signal_emit (panel, signals_p[READY], 0);
}

static void
bloom_panel_init_owner (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;

    if (!priv->proxy){
        g_warning (G_STRLOC " No DBus proxy!");
        return;
    }

    /*
    * Now call the remote init_panel() method to obtain the panel name, tooltip
    * and xid.
    */
    g_debug ("Calling InitPanel on %s %s", dbus_g_proxy_get_bus_name (priv->proxy),
                                           dbus_g_proxy_get_path (priv->proxy));

    org_agorabox_bloom_UX_Shell_Panel_init_panel_async (priv->proxy,
                                                        priv->width, priv->height,
                                                        bloom_panel_init_panel_reply_cb,
                                                        panel);
}

static DBusGConnection *
bloom_panel_connect_to_dbus ()
{
    DBusGConnection *conn;
    GError          *error = NULL;

    conn = dbus_g_bus_get (DBUS_BUS_SESSION, &error);

    if (!conn){
        g_warning ("Cannot connect to DBus: %s", error->message);
        g_error_free (error);
        return NULL;
    }

    return conn;
}

static void bloom_panel_dbus_proxy_weak_notify_cb (gpointer data, GObject *object)
{
    BloomPanel *panel = BLOOM_PANEL (data);
    if (bloom_panel_is_ready (panel)) {
        panel->priv->ready = FALSE;
        g_signal_emit(&(panel->parent), signals_p[REMOTE_PROCESS_DIED], 0, panel->priv->name);
        if (panel->priv->auto_rerun) {
            // Do nothing. D-Bus should do the work
        }
    }
}

void
bloom_panel_ping (DBusGConnection *dbus_conn,
                  gchar           *dbus_name)
{
    gchar           *p;
    gchar           *dbus_path;
    DBusGProxy      *proxy;

    dbus_path = g_strconcat ("/", dbus_name, NULL);

    p = dbus_path;
    while (*p){
        if (*p == '.')
            *p = '/';
        ++p;
    }

    proxy = dbus_g_proxy_new_for_name (dbus_conn,
                                       dbus_name,
                                       dbus_path, MPL_PANEL_DBUS_INTERFACE);

    dbus_g_proxy_set_default_timeout (proxy, PANEL_DEFAULT_TIMEOUT);

    g_debug("Spawing new instance of %s, %s by pinging it", dbus_name, dbus_path);
    org_agorabox_bloom_UX_Shell_Panel_ping_async (proxy,
                                                  bloom_panel_ping_cb,
                                                  NULL);

    // Don't really what happens if the proxy is freed...
    // g_object_unref (proxy);
}

static gboolean
bloom_panel_setup_proxy (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    DBusGProxy      *proxy;
    gchar           *dbus_path;
    gchar           *p;

    if (!priv->dbus_conn){
        g_warning (G_STRLOC " No dbus connection, cannot connect to panel!");
        return FALSE;
    }

    g_debug ("Creating proxy for %s, %p", priv->dbus_name, panel);

    dbus_path = g_strconcat ("/", priv->dbus_name, NULL);

    p = dbus_path;
    while (*p){
        if (*p == '.')
            *p = '/';
        ++p;
    }

    GError *error = NULL;

    /*
    proxy = dbus_g_proxy_new_for_name (priv->dbus_conn,
                                       priv->dbus_name,
                                       dbus_path, MPL_PANEL_DBUS_INTERFACE);
    */

    proxy = dbus_g_proxy_new_for_name_owner (priv->dbus_conn,
                                             priv->dbus_name,
                                             dbus_path,
                                             MPL_PANEL_DBUS_INTERFACE, &error);

    dbus_g_proxy_set_default_timeout (proxy, PANEL_DEFAULT_TIMEOUT);

    if (!proxy && error) {
        g_debug ("Unable to create proxy for " MPL_TOOLBAR_DBUS_PATH ": %s", error->message);
        g_error_free (error);
    }

    g_free (dbus_path);

    if (!proxy){
        g_warning ("Unable to create proxy for %s (reason unknown)",priv->dbus_name);
        return FALSE;
    }

    priv->proxy = proxy;

    dbus_g_proxy_add_signal (proxy, "RequestButtonStyle",G_TYPE_STRING, G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (proxy, "RequestButtonStyle",
                                 G_CALLBACK (bloom_panel_request_button_style_cb),
                                 panel, NULL);

    dbus_g_proxy_add_signal (proxy, "RequestTooltip",G_TYPE_STRING, G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (proxy, "RequestTooltip",
                                 G_CALLBACK (bloom_panel_request_tooltip_cb),
                                 panel, NULL);

    dbus_g_proxy_add_signal (proxy, "RequestHide",G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (proxy, "RequestHide",
                                 G_CALLBACK (bloom_panel_request_hide_cb_from_panel),
                                 panel, NULL);

    dbus_g_proxy_add_signal (proxy, "RequestShow",G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (proxy, "RequestShow",
                                 G_CALLBACK (bloom_panel_request_show_cb_from_panel),
                                 panel, NULL);

    bloom_panel_init_owner (panel);

    g_object_weak_ref (G_OBJECT (proxy),
            bloom_panel_dbus_proxy_weak_notify_cb, panel);

    return TRUE;
}

static void
bloom_panel_finalize (GObject *object)
{
    BloomPanelPrivate *priv = BLOOM_PANEL (object)->priv;

    g_free (priv->dbus_name);

    g_free (priv->name);
    g_free (priv->tooltip);
    g_free (priv->stylesheet);
    g_free (priv->button_style_id);

    G_OBJECT_CLASS (bloom_panel_parent_class)->finalize (object);
}

static void bloom_panel_class_init (BloomPanelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    g_type_class_add_private (klass, sizeof (BloomPanelPrivate));

    object_class->get_property     = bloom_panel_get_property;
    object_class->set_property     = bloom_panel_set_property;
    object_class->finalize         = bloom_panel_finalize;
    object_class->constructed      = bloom_panel_constructed;

    g_object_class_install_property (object_class,
                                     PROP_DBUS_NAME,
                                     g_param_spec_string ("dbus-name",
                                                          "Dbus name",
                                                          "Dbus name",
                                                          NULL,
                                                          G_PARAM_READWRITE |
                                                          G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_property (object_class,
                                     PROP_WIDTH,
                                     g_param_spec_uint ("width",
                                                        "Width",
                                                        "Width",
                                                        0, G_MAXUINT,
                                                        1024,
                                                        G_PARAM_READWRITE |
                                                        G_PARAM_CONSTRUCT));

    g_object_class_install_property (object_class,
                                     PROP_HEIGHT,
                                     g_param_spec_uint ("height",
                                                        "Height",
                                                        "Height",
                                                        0, G_MAXUINT,
                                                        1024,
                                                        G_PARAM_READWRITE |
                                                        G_PARAM_CONSTRUCT));

    signals_p[REQUEST_BUTTON_STYLE] =
      g_signal_new ("request-button-style",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, request_button_style),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);
    
    signals_p[REQUEST_TOOLTIP] =
      g_signal_new ("request-tooltip",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, request_tooltip),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);

    signals_p[REQUEST_SHOW] =
      g_signal_new ("request-show",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, request_show),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE,0);

    signals_p[SHOW_BEGIN] =
      g_signal_new ("show-begin",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, show_begin),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);

    signals_p[SHOW_COMPLETE] =
      g_signal_new ("show-complete",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, show_complete),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);

    signals_p[REQUEST_UNACTIVE_BUTTONS] =
      g_signal_new ("request-unactive-buttons",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, request_unactive_buttons),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);

    signals_p[REQUEST_ACTIVE_BUTTONS] =
      g_signal_new ("request-active-buttons",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, request_active_buttons),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);

    signals_p[READY] =
      g_signal_new ("ready",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, ready),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);

    signals_p[REMOTE_PROCESS_DIED] =
      g_signal_new ("remote-process-died",
                    G_TYPE_FROM_CLASS (object_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (BloomPanelClass, remote_process_died),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);

    dbus_g_object_register_marshaller (moblin_netbook_marshal_VOID__UINT_UINT,
                                     G_TYPE_NONE,
                                     G_TYPE_UINT, G_TYPE_UINT, G_TYPE_INVALID);

    klass->instance = NULL;
}


static void
bloom_panel_init (BloomPanel *self)
{
    BloomPanelPrivate *priv;

    priv = self->priv = BLOOM_PANEL_GET_PRIVATE (self);
}


static void
bloom_panel_constructed (GObject *self)
{
    BloomPanelPrivate *priv = BLOOM_PANEL (self)->priv;
    DBusGConnection *conn;

    /*
    * Make sure our parent gets chance to do what it needs to.
    */
    if (G_OBJECT_CLASS (bloom_panel_parent_class)->constructed)
    G_OBJECT_CLASS (bloom_panel_parent_class)->constructed (self);

    if (!priv->dbus_name || !g_strcmp0(priv->dbus_name, "")) {
        return;
    }

    conn = bloom_panel_connect_to_dbus ();

    if (!conn){
        g_warning (G_STRLOC " Unable to connect to DBus!");
        return;
    }

    priv->dbus_conn = conn;

    if (!bloom_panel_setup_proxy (BLOOM_PANEL (self)))
        return;

    priv->constructed = TRUE;
}

BloomPanel *
bloom_panel_new (const gchar  *dbus_name,
                 guint         width,
                 guint         height,
                 gboolean      auto_rerun)
{
    BloomPanel *panel = g_object_new (BLOOM_TYPE_PANEL,
                                      "dbus-name", dbus_name,
                                      "width", width,
                                      "height", height,
                                      NULL);

    panel->priv->auto_rerun = auto_rerun;

    if (!panel->priv->constructed) {
        g_warning (G_STRLOC " Construction of Panel for %s failed.", dbus_name);
        return NULL;
    }

    return panel;
}

const gchar *
bloom_panel_get_name (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    return priv->name;
}

guint
bloom_panel_get_pid (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    return priv->pid;
}

const gchar *
bloom_panel_get_tooltip (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    return priv->tooltip;
}

const gchar *
bloom_panel_get_stylesheet (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    return priv->stylesheet;
}

const gchar *
bloom_panel_get_button_style (BloomPanel *panel)
{
    BloomPanelPrivate *priv = panel->priv;
    return priv->button_style_id;
}

gboolean
bloom_panel_is_ready (BloomPanel *panel)
{
    return panel->priv->ready;
}

gboolean
bloom_panel_is_shown (BloomPanel *panel)
{
    return panel->priv->show;
}
