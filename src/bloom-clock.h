/*
 * bloom-clock.h
 *
 *  Created on: 30 july 2010
 *      Author: Josselin Roger
 */

#ifndef BLOOM_CLOCK_H_
#define BLOOM_CLOCK_H_

#include <QtGui>

class BloomTooltip;

class BloomClock : public QLabel
{
    Q_OBJECT

public:
    BloomClock (QWidget * parent);

public slots:
    void update_time ();

protected:
    void enterEvent (QEvent *event);
    void leaveEvent (QEvent *event);

    BloomTooltip *tooltip;
    QTimer timer;
};

#endif /* BLOOM_CLOCK_H_ */
