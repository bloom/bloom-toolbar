/*
 * WindowManager.cpp
 *
 *  Created on: 19 july 2010
 *      Author: Josselin Roger
 */

#include "window-manager.h"
#include "qt-toolbar.h"

#include <X11/Xlib.h>
#include <sys/types.h>
#include <unistd.h>

#define MAXPIDLEN 5
#define MAXPROCPATHLEN (MAXPIDLEN + 11 + 1)

#define SCROLL_ANIMATION_DURATION 20
#define SCROLL_SWITCH_DURATION 1000
#define POPUP_ANIMATION_DURATION 200

#define VERTICAL_SCROLL_OFFSET 10

WindowManager *current = 0;

static void window_opened_cb (WnckScreen *screen, WnckWindow *window);
static void window_closed_cb (WnckScreen *screen, WnckWindow *window);
static void active_window_changed_cb (WnckScreen *screen,
                                      WnckWindow *previously_active_window,
                                      gpointer    user_data);
static void window_stacking_changed_cb (WnckScreen *screen,
                                        gpointer    user_data);
static pid_t parent_pid (pid_t pid);

WindowEntry::WindowEntry (WindowManager *parent, QString window, int _index, QPixmap pixmap) :
    QWidget(parent),
    label(window)
{
    index = _index;
    wm = parent;
    setObjectName("windowlist");

    icon.resize (pixmap.size ());
    icon.setPixmap (pixmap);
    icon.setContentsMargins (0, 0, 0, 0);
    icon.setMouseTracking (true);

    required_width = fontMetrics ().width (label.text ());
    label.resize(700, icon.height () + 2);
    label.setContentsMargins (0, 0, 5, 0);
    label.setMouseTracking (true);

    layout.setContentsMargins (5, 0, 5, 0);
    layout.setSpacing (5);
    layout.addWidget (&icon);
    layout.addWidget (&label);
    layout.setAlignment (Qt::AlignLeft);

    setLayout (&layout);

    int height = MAX (icon.height(), label.height());
    resize (MENU_WIDTH, height);

    setMouseTracking (true);

    switching = false;
    scrolling_left = true;

    animated_label_timer = new QTimer (this);
    connect(animated_label_timer, SIGNAL (timeout ()), this, SLOT (animate_label ()));

    highlight (false);
}

void WindowEntry::highlight (bool state)
{
    if (state) {
        label.setObjectName ("highlighted");
        icon.setObjectName ("highlighted");
    }
    else {
        label.setObjectName ("unhighlighted");
        icon.setObjectName ("unhighlighted");
    }

    // Seems to be required for Qt to refresh the style
    setStyleSheet (styleSheet ());
}

void WindowEntry::animate_label ()
{
    int offset = label.contentsMargins ().left ();

    if (switching) {
        switching = false;
        animated_label_timer->setInterval (SCROLL_ANIMATION_DURATION);
    }

    if ((scrolling_left && offset < width () - required_width) ||
        (!scrolling_left && offset > 0)) {
        animated_label_timer->setInterval (SCROLL_SWITCH_DURATION);
        switching = true;
        scrolling_left = !scrolling_left;
    }
    else {
        label.setContentsMargins (offset + (scrolling_left ? -1 : 1 ), 0, 5, 0);
    }
}

void WindowEntry::enterEvent (QEvent *event)
{
    highlight (true);

    if (required_width >= label.width())
        animated_label_timer->start (SCROLL_ANIMATION_DURATION);
}

void WindowEntry::leaveEvent (QEvent *event)
{
    highlight (false);
    animated_label_timer->stop ();
    label.setContentsMargins (0, 0, 5, 0);
}

void WindowEntry::mouseReleaseEvent (QMouseEvent *event)
{
    wm->select (index);
}

WindowManager::WindowManager(Toolbar *parent)
    : QWidget(parent)
{
    current = this;
    toolbar = parent;
    main_layout = NULL;
    folding = false;
    inhibited = true;

    setObjectName ("windowlist");
    setWindowFlags (Qt::ToolTip);
    move (1, TOOLBAR_HEIGHT + 1);
    setAttribute (Qt::WA_TranslucentBackground, true);
    setWindowOpacity (0.9);

    /*
     * Create two buttons in case there are many displayed windows
     */
    scroll_plus = new QPushButton (this);
    scroll_plus->resize (MENU_WIDTH, 20);
    scroll_plus->setFocusPolicy (Qt::NoFocus);
    scroll_plus->setObjectName ("scroll-plus");
    scroll_plus->hide ();

    scroll_minus = new QPushButton (this);
    scroll_minus->resize (MENU_WIDTH, 20);
    scroll_minus->setFocusPolicy (Qt::NoFocus);
    scroll_minus->setObjectName ("scroll-minus");
    scroll_minus->hide ();

    connect (scroll_plus, SIGNAL (pressed ()), this, SLOT (scroll ()));
    connect (scroll_minus,SIGNAL (pressed ()), this, SLOT (scroll ()));
    connect (scroll_plus, SIGNAL (released ()), this, SLOT (stop_scrolling ()));
    connect (scroll_minus, SIGNAL (released ()), this, SLOT (stop_scrolling ()));

    scroll_area = new QScrollArea (this);
    scroll_area->setAlignment (Qt::AlignVCenter);
    QDesktopWidget *desktop = QApplication::desktop ();
    QRect screen_rect = desktop->screenGeometry (desktop->primaryScreen ());
    setMaximumSize (screen_rect.width (), screen_rect.height () - TOOLBAR_HEIGHT);
    scroll_area->setMaximumSize (screen_rect.size ());

    area = new QWidget (this);
    area->setObjectName ("area");
    scroll_area->setWidget (area);
    scroll_area->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
    scroll_area->setVerticalScrollBarPolicy (Qt::ScrollBarAlwaysOff);

    // Set up Wnck callbacks
    const int num_screens = gdk_display_get_n_screens (gdk_display_get_default ());
    for (int i = 0 ; i < num_screens; ++i) {
        WnckScreen *screen = wnck_screen_get (i);

        g_signal_connect (screen, "window-opened", (GCallback) window_opened_cb, NULL);
        g_signal_connect (screen, "window-closed", (GCallback) window_closed_cb, NULL);
        g_signal_connect (screen, "active-window-changed", (GCallback) active_window_changed_cb, NULL);
        g_signal_connect (screen, "window-stacking-changed", (GCallback) window_stacking_changed_cb, NULL);
    }

    setMouseTracking (true);

    scrolling_timer = new QTimer (this);
    scrolling_timer->setSingleShot (true);
    connect(scrolling_timer, SIGNAL (timeout()), this, SLOT (scroll ()));

    // Set up the 'popup' animation
    popup_animation = new QPropertyAnimation (this, "geometry", this);
    popup_animation->setDuration (POPUP_ANIMATION_DURATION);
    popup_animation->connect (popup_animation, SIGNAL (finished ()), this, SLOT (animation_finished ()));

    hide_timer = new QTimer ();
    hide_timer->connect (hide_timer, SIGNAL (timeout ()), this, SLOT (hide ()));
    hide_timer->setSingleShot (true);

    inhibited = false;
}

void WindowManager::animation_finished ()
{
    if (!folding)
        QWidget::hide ();
}

void WindowManager::show ()
{
    if (isHidden () && entries.size () > 0) {
        folding = true;
        popup_animation->setStartValue (QRect (pos ().x (), pos ().y (), 0, 0));
        popup_animation->setEndValue (QRect (pos ().x(), pos ().y(), width (), height ()));
        popup_animation->start ();
        update ();
        resize (0, 0);
        QWidget::show ();
    }
}

void WindowManager::hide ()
{
    stop_hide_timer ();
    if (isVisible ()) {
        folding = false;
        popup_animation->setStartValue (QRect (pos ().x (), pos ().y (), width (), height ()));
        popup_animation->setEndValue (QRect (pos ().x(), pos ().y(), 0, 0));
        popup_animation->start ();
    }
}

QPixmap WindowManager::get_icon (WnckWindow *window)
{
    GdkPixbuf *pixbuf = wnck_window_get_icon (window);
    int x, y;
    int width, height, rowstride, n_channels;
    guchar *pixels, *p;

    n_channels = gdk_pixbuf_get_n_channels (pixbuf);
    width = gdk_pixbuf_get_width (pixbuf);
    height = gdk_pixbuf_get_height (pixbuf);

    rowstride = gdk_pixbuf_get_rowstride (pixbuf);
    pixels = gdk_pixbuf_get_pixels (pixbuf);

    uchar *buffer = new uchar[(width*height)*4];
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            p = pixels + y * rowstride + x * n_channels;
            buffer[(y * width + x) * 4 + 0] = p[2];
            buffer[(y * width + x) * 4 + 1] = p[1];
            buffer[(y * width + x) * 4 + 2] = p[0];
            buffer[(y * width + x) * 4 + 3] = p[3];
        }
    }

    QImage img = QImage (buffer, width, height, 4 * width, QImage::Format_ARGB32);
    QPixmap pix;
    return pix.fromImage (img);
}

void WindowManager::update()
{
    QDesktopWidget *desktop = QApplication::desktop ();
    QRect screen_rect = desktop->screenGeometry (desktop->primaryScreen ());
    setMaximumSize (screen_rect.width (), screen_rect.height () - TOOLBAR_HEIGHT);
    scroll_area->setMaximumSize (screen_rect.width (), screen_rect.height () - TOOLBAR_HEIGHT);

    if (!main_layout || (main_layout && !main_layout->isEmpty())) {
        delete main_layout;
        main_layout = new QVBoxLayout ();
        main_layout->setSizeConstraint (QLayout::SetNoConstraint);
        main_layout->setAlignment (Qt::AlignTop);
        main_layout->setContentsMargins (0, 0, 0, 0);
        main_layout->setSpacing (4);
        area->setLayout (main_layout);
    }

    for (int i = entries.length() - 1; i >= 0; i--) {
        delete entries[i];
    }
    entries.clear();
    
    int total_height = 0;
    for (int i = 0; i < w_list.length (); i++) {
        WindowEntry *entry = new WindowEntry (this,
                                              QString::fromUtf8 (wnck_window_get_name (w_list.at (i))),
                                              i,
                                              get_icon (w_list.at (i)));
        entries.append (entry);

        total_height += entry->height ();
        main_layout->addWidget (entry);
    }

    resize (MENU_WIDTH, total_height + 12);
    scroll_area->resize (MENU_WIDTH, total_height + 12);
    area->resize (MENU_WIDTH, total_height + 2);

    scroll_update ();

    if (!w_list.length ()) {
        toolbar->show_home_panel_if_needed ();
    }
}

void WindowManager::scroll_update ()
{
    int voffset = scroll_area->verticalScrollBar ()->value ();
    int minimum = scroll_area->verticalScrollBar ()->minimum ();
    int maximum = scroll_area->verticalScrollBar ()->maximum ();

    if (voffset != minimum) {
        scroll_plus->show ();
        scroll_plus->raise ();
        scroll_plus->move (0, 0);
    } else {
        scroll_plus->setDown (false);
        scroll_plus->hide ();
    }
    if (voffset != maximum) {
        scroll_minus->show ();
        scroll_minus->raise ();
        scroll_minus->move (0, height () - scroll_minus->height ());
    } else {
        scroll_minus->setDown (false);
        scroll_minus->hide ();
    }
}

void WindowManager::display ()
{
    if (isHidden()) {
       update();
       show();
    } else {
        hide();
    }
}

void WindowManager::highlight_window (int index, bool state)
{
    entries[index]->highlight (state);
}

void WindowManager::select (int index)
{
    wnck_window_activate (w_list.at (index), gtk_get_current_event_time ());
    toolbar->bloom_toolbar_hide_all ();
    hide ();
}

void WindowManager::animate_logo ()
{
    toolbar->animate_logo ();
}

void WindowManager::wheelEvent (QWheelEvent *event)
{
    QWidget::wheelEvent (event);
    scroll_update ();
}

void WindowManager::enterEvent (QEvent *event)
{
    stop_hide_timer ();
}

void WindowManager::leaveEvent (QEvent *event)
{
    start_hide_timer ();
}

void WindowManager::scroll ()
{
    int voffset = scroll_area->verticalScrollBar ()->value ();

    /* scrolling activated by buttons */
    if (QApplication::mouseButtons () == Qt::LeftButton) {
        if (scroll_plus->isDown ()) {
            scroll_area->verticalScrollBar ()->setValue (voffset - VERTICAL_SCROLL_OFFSET);
        } else if (scroll_minus->isDown ()) {
            scroll_area->verticalScrollBar ()->setValue (voffset + VERTICAL_SCROLL_OFFSET);
        }
        scrolling_timer->start (scrolling_speed);
    }

    scroll_update();

    if (scrolling_speed == 300) {
        if (scroll_area->horizontalScrollBar ()->maximum () != scroll_area->horizontalScrollBar ()->minimum ())
            scrolling_speed = 25000 / scroll_area->horizontalScrollBar ()->pageStep ();
        else
            scrolling_speed = 25000 / scroll_area->verticalScrollBar ()->pageStep ();
    }
}

void WindowManager::stop_scrolling ()
{
    /* force the stop scrolling */
    scrolling_timer->stop ();
    scrolling_speed = 300;
}

void WindowManager::hide_all_panels ()
{
    toolbar->bloom_toolbar_hide_all ();
}

bool WindowManager::is_panel_child (WnckWindow *window)
{
    /*
     * Iterate through all the panels to check if the window
     * is a child of a panel
     */

    if (getpid () == wnck_window_get_pid (window))
        return true;

    if (wnck_window_get_application (window) &&
        wnck_application_get_name (wnck_window_get_application (window)) &&
        !strcmp(wnck_application_get_name (wnck_window_get_application (window)), "Banshee"))
        return true;

    for (GSList *pids = toolbar->get_pids_panels (); pids; pids = pids->next) {
        guint panel_pid = (guint) pids->data;
        unsigned int window_pid = wnck_window_get_pid (window);

        while (window_pid != 1 && window_pid != 0) {
            if (panel_pid == window_pid) {
                return true;
            }
            window_pid = parent_pid (window_pid);
        }
    }

    return false;
}

bool WindowManager::is_inhibited ()
{
    return inhibited;
}

void WindowManager::inhibit (bool state)
{
    inhibited = state;
}

void WindowManager::flush ()
{
    inhibit (true);
    while (gtk_events_pending ())
        gtk_main_iteration ();
    inhibit (false);
}

void WindowManager::start_hide_timer ()
{
    hide_timer->start (500);
}

void WindowManager::stop_hide_timer ()
{
    hide_timer->stop ();
}

void display_window (WnckWindow *window)
{
    qDebug () << " Window" << window;
    if (window) {
        qDebug () << "  Name:" << (wnck_window_get_application (window) ? wnck_application_get_name (wnck_window_get_application (window)) : "");
        qDebug () << "  Type:" << wnck_window_get_window_type(window);
        qDebug () << "  Pid:" << wnck_window_get_pid (window);
        qDebug () << "  Xid:" << wnck_window_get_xid (window);
        qDebug () << "  Active:" << wnck_window_is_active (window);
        qDebug () << "  Above:" << wnck_window_is_above (window);
        qDebug () << "  Is a panel:" << current->is_panel_child (window);

    }
}

static void window_opened_cb (WnckScreen *screen, WnckWindow *window)
{
    if (wnck_window_get_pid (window) == 0)
        return;

#ifdef DEBUG_WINDOW_MANAGER
    qDebug () << "window_opened_cb";
    display_window (window);
#endif

    if (current->is_panel_child (window))
        wnck_window_activate (window, gtk_get_current_event_time ());

    if (wnck_window_get_window_type (window) == 0 &&
        !wnck_window_is_skip_tasklist (window) &&
        !current->is_panel_child (window)) {
        current->add_window (window);
#ifdef DEBUG_WINDOW_MANAGER
        qDebug () << "Hiding all panels from window_opened_cb";
#endif
        if (!current->is_inhibited ()) {
            current->animate_logo ();
            current->hide_all_panels ();
        }
    }
}

static void window_closed_cb(WnckScreen *screen, WnckWindow *window)
{
#ifdef DEBUG_WINDOW_MANAGER
    qDebug () << "window_closed_cb";
    display_window (window);
#endif
    for (int i = 0; i < current->get_nbr_windows (); i++)
        if (wnck_window_get_xid(window) == wnck_window_get_xid (current->get_window (i)))
            current->remove_window (i);

    current->update();
}

static void
active_window_changed_cb (WnckScreen *screen,
                          WnckWindow *previously_active_window,
                          gpointer    user_data)
{
    WnckWindow * window = NULL;
    WnckWindow * stacked = NULL;
    GList *element = NULL;

    window = (WnckWindow *) wnck_screen_get_active_window (screen);

#ifdef DEBUG_WINDOW_MANAGER
    qDebug () << "active_window_changed_cb:";
    display_window (window);
    qDebug () << "previous";
    display_window (previously_active_window);
#endif

    if (!wnck_screen_get_active_window (screen))
        return;

    for (element = g_list_last(wnck_screen_get_windows_stacked (screen));
        element && wnck_window_get_window_type((WnckWindow *) element->data) == 0;
        element = element->prev) {
    }

    if (element) {
        stacked = (WnckWindow *) element->data;
    }

#ifdef DEBUG_WINDOW_MANAGER
    qDebug () << "stacked:";
    display_window (stacked);
#endif

    if (!current->is_inhibited () &&
        (wnck_window_get_window_type(window) == 0 ||
         wnck_window_get_window_type(window) == 2)) {
        if (!current->is_panel_child (window)) {
#ifdef DEBUG_WINDOW_MANAGER
            qDebug () << "Hiding all panels from active_window_changed_cb";
#endif
            current->hide_all_panels();
        }
    }
}

static void
window_stacking_changed_cb (WnckScreen *screen,
                            gpointer    user_data)
{
#ifdef DEBUG_WINDOW_MANAGER
    qDebug () << "window-stacking-changed";
#endif
    for (GList *element = wnck_screen_get_windows_stacked (screen); element != NULL; element = element->next) {
        display_window ((WnckWindow *) element->data);
    }
}

pid_t parent_pid (pid_t pid)
{
    pid_t ppid;
    char procpath[MAXPROCPATHLEN];
    FILE *procstat;

    if (pid == 0)
        return 0;

    snprintf (procpath, MAXPROCPATHLEN, "/proc/%u/stat", pid);
    procstat = fopen (procpath, "r");
    if (procstat <= 0) {
        qDebug () << "Failed to open" << procpath;
        perror ("fopen(procstat)");
        return 0;
    }
    fscanf (procstat, "%*d %*s %*c %u", &ppid);
    fclose (procstat);
    return ppid;
}

void
inhibit_window_manager (unsigned char state)
{
    current->inhibit (state);
}

void
flush_window_manager (unsigned char state)
{
    current->flush ();
}

