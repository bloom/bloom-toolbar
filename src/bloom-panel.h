/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */

/* bloom-panel.h */
/*
 * Copyright (c) 2009, 2010 Agorabox.
 *
 * Authors: Bastien Bouzerau <bastien.bouzerau@agorabox.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _BLOOM_PANEL
#define _BLOOM_PANEL

#include <glib-object.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
#include "../libbloom-toolbar/libbloom-toolbar.h"

#define BLOOM_TYPE_PANEL                 (bloom_panel_get_type())
#define BLOOM_PANEL(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), BLOOM_TYPE_PANEL, BloomPanel))
#define BLOOM_PANEL_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), BLOOM_TYPE_PANEL, BloomPanelClass))
#define BLOOM_IS_PANEL(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BLOOM_TYPE_PANEL))
#define BLOOM_IS_PANEL_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), BLOOM_TYPE_PANEL))
#define BLOOM_PANEL_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), BLOOM_TYPE_PANEL, BloomPanelClass))

enum
{
    READY,
    REQUEST_BUTTON_STYLE,
    SET_GDK_ID,
    REQUEST_TOOLTIP,
    REQUEST_UNACTIVE_BUTTONS,
    REQUEST_ACTIVE_BUTTONS,
    REQUEST_SHOW,
    REQUEST_HIDE,
    SHOW_BEGIN,
    SHOW_COMPLETE,
    CLICKED,
    REMOTE_PROCESS_DIED,
    SET_SIZE,
    LAST_SIGNAL
};

typedef struct _BloomPanelPrivate BloomPanelPrivate;

typedef struct 
{
    GObject parent;
    BloomPanelPrivate *priv;
} BloomPanel;

typedef struct 
{
    GObjectClass parent_class;
    GObject *instance;
    void (*ready)                     (BloomPanel *panel);
    void (*show_begin)                (BloomPanel *panel);
    void (*show_complete)             (BloomPanel *panel);
    void (*request_button_style)      (BloomPanel *panel, const gchar *style);
    void (*set_gdk_id)                (BloomPanel *panel, const int xid);
    void (*request_tooltip)           (BloomPanel *panel, const gchar *tooltip);
    void (*request_unactive_buttons)  (BloomPanel *panel);
    void (*request_active_buttons)    (BloomPanel *panel);
    void (*request_show)              (BloomPanel *panel);
    void (*request_hide)              (BloomPanel *panel);
    void (*remote_process_died)       (BloomPanel *panel);
} BloomPanelClass;

GType        bloom_panel_get_type (void);
const gchar *bloom_panel_get_name (BloomPanel *panel);
guint        bloom_panel_get_pid (BloomPanel *panel);
gboolean     bloom_panel_is_ready (BloomPanel *panel);
gboolean     bloom_panel_is_shown (BloomPanel *panel);
void         bloom_panel_request_show_cb (BloomPanel *panel);
void         bloom_panel_request_hide_cb (BloomPanel *panel);
void         bloom_panel_resize (BloomPanel *panel, int width, int height);
void         bloom_panel_ensure_position (BloomPanel *panel);
void         bloom_panel_move (BloomPanel *panel, int x, int y);
void         bloom_panel_ping (DBusGConnection *dbus_conn, gchar *dbus_name);
guint        bloom_panel_get_pid (BloomPanel *panel);
const gchar *bloom_panel_get_tooltip (BloomPanel *panel);
const gchar *bloom_panel_get_stylesheet (BloomPanel *panel);
const gchar *bloom_panel_get_button_style (BloomPanel *panel);
BloomPanel  *bloom_panel_new (const gchar  *dbus_name,
                              guint         width,
                              guint         height,
                              gboolean      auto_rerun);

#endif
