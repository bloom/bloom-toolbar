/*
 * bloom-clock.cpp
 *
 *  Created on: 30 july 2010
 *      Author: Josselin Roger
 */

#include "bloom-clock.h"
#include "bloom-tooltip.h"

BloomClock::BloomClock (QWidget * parent)
    : QLabel (parent)
{
    setObjectName ("clock");

    tooltip = new BloomTooltip (this);
    tooltip->setObjectName ("tooltip");
    tooltip->setAttribute (Qt::WA_TranslucentBackground, true);

    timer.connect(&timer, SIGNAL (timeout ()),this, SLOT (update_time ()));
    timer.start (1000);

    update_time ();
}

void BloomClock::enterEvent (QEvent * event)
{
    tooltip->set_tooltip (QDate::currentDate ().toString (Qt::SystemLocaleLongDate));
    tooltip->display ();
}

void BloomClock::leaveEvent (QEvent *event)
{
    tooltip->hide ();
}

void BloomClock::update_time ()
{
    setText (QTime::currentTime ().toString ("hh : mm"));
}
