#include <QApplication>
#include <QtGui>
#include "qt-toolbar.h"
#include <glib-object.h>
#include <glib.h>

int main (int argc, char *argv[])
{
    gtk_init (&argc, &argv);
    QApplication app (argc, argv);

    /*
     * Load CSS stylesheet
     */
    QFile file (THEMEDIR"/style.css");
    if (file.open (QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream stream (&file);
        QString sheet = stream.readAll ();
        sheet.replace ("THEMEDIR", THEMEDIR);
        app.setStyleSheet (sheet);
        file.close ();
    }

    Toolbar window;
    window.show ();

    return app.exec ();
}

