#include "bloom-tooltip.h"
#include <algorithm>

#define TOOLTIP_MINIMUM_SIZE 10

BloomTooltip::BloomTooltip(QWidget *parent) :
    QWidget (parent, Qt::Tool | Qt::X11BypassWindowManagerHint)
{
    setObjectName ("tooltip");
    setAttribute(Qt::WA_TranslucentBackground, true);
    
    QVBoxLayout* tooltip_layout = new QVBoxLayout (this);
    tooltip_layout->setContentsMargins (0, 0, 0, 0);
    tooltip_layout->setSpacing (0);

    arrow = new QLabel (this);
    arrow->setObjectName ("arrow");
    tooltip_layout->addWidget (arrow);

    label = new QLabel (this);
    label->setObjectName ("tooltiplabel");
    label->setAlignment (Qt::AlignCenter);
    label->setScaledContents (true);
    tooltip_layout->addWidget (label);

    setLayout(tooltip_layout);
}

void BloomTooltip::display ()
{
    // The first time 'display' is called, width () returns
    // a size that is smaller than the required size
    // ... so we compute it
    int required_width = label->fontMetrics ().width (label->text ()) +
                         label->contentsMargins ().right ();

    QPoint global (parentWidget ()->mapToGlobal (QPoint ((parentWidget ()->width () - required_width) / 2,
                                                         parentWidget ()->height ())));
    QDesktopWidget *desktop = QApplication::desktop ();
    int screen_width = desktop->availableGeometry ().width ();
    int out_width = std::max (0, global. x() + required_width - screen_width);

    label->resize (required_width, height ());
    move (global.x () - out_width, global.y ());

    qDebug () << label->contentsMargins ().right () << global.x () << width () << label->width () << required_width << screen_width << out_width;
    QString style ("margin-left: %1;");
    style = style.arg(out_width * 2);
    arrow->setStyleSheet (style);

    show ();
}

void BloomTooltip::set_tooltip (QString text)
{
    label->setText (" " + text + " ");
}

