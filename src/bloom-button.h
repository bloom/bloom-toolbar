/* Class to manage events like mouseEnterEvent on Bloom button Toolbar */

#ifndef BLOOM_BUTTON
#define BLOOM_BUTTON

#include <QtGui>

#include <QMouseEvent>
#include <glib.h>
#include <glib-object.h>

class Toolbar;
class BloomTooltip;

class BloomButton : public QPushButton
{
    Q_OBJECT

public:
    BloomButton (QWidget *parent = 0, QString name = 0, bool boolean = true);

    void show_tooltip ();
    void set_tooltip (QString tooltip);
    void set_active (bool);
    void reposition ();
    void set_pixmaps (QPixmap inactive, QPixmap active);

protected:
    void enterEvent (QEvent*);
    void leaveEvent (QEvent*);
    void mousePressEvent (QMouseEvent*);
    void mouseReleaseEvent (QMouseEvent*);

public:
    Toolbar *toolbar;
    QLabel *icon;
    QLabel *icon_active;
    BloomTooltip *tooltip;
    QPropertyAnimation *enter_animation;
    QPropertyAnimation *pressed_animation;
    QPropertyAnimation *released_animation;

    bool is_panel;
    bool active;
};

#endif
