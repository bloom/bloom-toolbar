/*
 * windows_manager.h
 *
 *  Created on: 19 july 2010
 *      Author: Josselin Roger
 */

#ifndef WINDOWS_MANAGER_H_
#define WINDOWS_MANAGER_H_

#include <QtGui>

#define __GTK_BINDINGS_H__
#define WNCK_I_KNOW_THIS_IS_UNSTABLE
#include <libwnck/libwnck.h>

#define MENU_WIDTH 300

extern "C" 
{
    #include "bloom-toolbar.h"
}

class Toolbar;
class WindowManager;

class WindowEntry : public QWidget
{
    Q_OBJECT

public:
    WindowEntry(WindowManager *parent, QString window, int index, QPixmap pixmap);
    void highlight (bool);
    void animate_logo ();

public slots:
    void animate_label();

    void enterEvent (QEvent *event);
    void leaveEvent (QEvent *event);
    void mouseReleaseEvent (QMouseEvent *event);

public:
    WindowManager *wm;

protected:
    QLabel label;
    QLabel icon;
    QHBoxLayout layout;
    QTimer *animated_label_timer;

    int index;
    int required_width;
    bool scrolling_left;
    bool switching;
};

class WindowManager : public QWidget
{
    Q_OBJECT

public:
    WindowManager       (Toolbar *parent);
    void update();
    int                 get_nbr_windows () { return w_list.length(); }
    WnckWindow*         get_window (int index) { return w_list.at(index); }
    void                add_window( WnckWindow *window) { w_list.append(window); }
    void                remove_window (int index) { w_list.removeAt(index); }
    void                animate_logo ();
    void                hide_all_panels ();
    void                highlight_window (int, bool);
    GSList              *get_all_pids_panels ();
    QPixmap             get_icon (WnckWindow*);
    bool                is_panel_child (WnckWindow*);
    void                start_hide_timer ();
    void                stop_hide_timer ();
    void                display ();
    void                select (int);
    bool                is_inhibited ();
    void                inhibit (bool);
    void                flush ();

protected:
    void enterEvent (QEvent *event);
    void leaveEvent (QEvent *event);
    void setMouseTrackingAllChildren (QWidget* widget);
    void wheelEvent (QWheelEvent *);
    void scroll_update ();

public slots:
    void show();
    void hide();

protected slots:
    void scroll ();
    void stop_scrolling ();
    void animation_finished ();

private:
    Toolbar             *toolbar;
    QList<WindowEntry*> entries;
    QList<WnckWindow*>  w_list;
    QVBoxLayout         *main_layout;
    QScrollArea         *scroll_area;
    QWidget             *area;
    QPushButton         *scroll_minus;
    QPushButton         *scroll_plus;
    QTimer              *scrolling_timer;
    int                 scrolling_speed;
    WindowEntry         *selection;
    QTimer              *hide_timer;
    QTimer              *show_timer;
    QPropertyAnimation  *popup_animation;
    gboolean             folding;
    gboolean             inhibited;
};

#endif /* WINDOWS_MANAGER_H_ */
