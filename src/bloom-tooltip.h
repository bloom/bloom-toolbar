#ifndef BLOOM_TOOLTIP
#define BLOOM_TOOLTIP

#include <QtGui>
#include <QtCore>

class BloomTooltip : public QWidget
{
public:
    BloomTooltip (QWidget*);

    void display ();
    void set_tooltip (QString text);

    QLabel *arrow;
    QLabel *label;
};

#endif

