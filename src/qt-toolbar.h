#ifndef HEADER_TOOL_BAR
#define HEADER_TOOL_BAR

#include <QtGui>
#include "bloom-button.h"
#include <glib-object.h>
#include <glib.h>
#include "window-manager.h"
#include "bloom-clock.h"
#include "../libbloom-toolbar/libbloom-toolbar.h"

extern "C" 
{
    #include "bloom-toolbar.h"
    #include "bloom-panel.h"
    #include <moblin-panel/mpl-panel-common.h>
}

extern guint signals_tb[];

class BloomMainButton;

class Toolbar : public QWidget
{
    Q_OBJECT

    public:
        Toolbar             (QWidget *parent = 0);
        void                 bloom_toolbar_hide_all ();
        void                 bloom_toolbar_request_button_style_cb ();
        void                 bloom_toolbar_toggled_button (QObject *source);
        void                 bloom_toolbar_unactive_button (const gchar *name);
        void                 bloom_toolbar_active_button (const gchar *name);
        BloomButton*         bloom_add_button (const gchar  *name);
        int                  current_time_msec ();
        void                 animate_logo ();
        void                 show_home_panel_if_needed ();
        BloomToolbar         *toolbar;
        QList <BloomButton*> list_panels;
        QList <QWidget *>    list_separators;
        guint                wid;
        BloomButton          *table_panel[NUM_ZONES];
        GSList               *get_pids_panels ();

    protected:
        void showEvent         (QShowEvent*);

    private slots:
        void screen_resize (int);

    signals:
        void clicked ();
        void workAreaResized (int);

    private:
        WindowManager         *wm;
        BloomClock            *clock;
        QTime                  time;
        QString                time_string;
        QLabel                *current_date;
        QDate                  date;
        QString                date_string;
        QDesktopWidget        *desktop;
        QRect                  screen_rect;
        QPropertyAnimation    *anim;
        BloomMainButton       *logo;
        QMovie                *logo_anim;
        QHBoxLayout           *main_layout;
        QWidget               *background;
        QWidget               *back_widget;
        QTimer                *size_changed_timer;

    public:
        static Toolbar        *instance;
};

extern "C" {
    void request_button_style_type (BloomToolbar *toolbar, gpointer data, Toolbar *tb);
    void request_tooltip (BloomToolbar *toolbar, gpointer data, Toolbar *tb);
    void request_unactive_buttons (BloomToolbar *toolbar, gchar *name, Toolbar *tb);
    void request_active_buttons (BloomToolbar *toolbar, gchar *name, Toolbar *tb);
    void request_button_clicked (BloomToolbar *toolbar, gchar *name, Toolbar *tb);
    void bloom_toolbar_show ();
    void bloom_toolbar_hide ();
}

#endif
